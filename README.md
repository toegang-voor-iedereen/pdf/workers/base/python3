# PDF Processor Base Worker in Python3

To create a worker using python3, we supply this repository as the foundation for that worker.

This package does the following basic tasks for the worker:

- Connects to AMQP and listens for jobs
- For each new job, decodes the message and downloads the corresponding file from MinIO
- Calls the jobHandler method you supply
- `Ack`s the message to the station


## How to use

### Include the package

To use this package, simply add the following to your requirements.txt;

```
--extra-index-url https://gitlab.com/api/v4/projects/48255673/packages/pypi/simple

kimiworker
```

and run the following command:

```
pip install -r requirements.txt
```

And add the following to your Dockerfile:

```
# syntax=docker/dockerfile:1
FROM python:3.11-slim
COPY requirements.txt requirements.txt
RUN pip3 install -r requirements.txt
COPY . .
CMD ["python3", "-u", "main.py"]
```

### Create the main script

In you main file i.e. `main.py`, include the package, create the worker with a job handler callback function, and start the worker:

```
import kimiworker

def job_handler(logger: KimiLogger, job: WorkerJob, job_id: str, local_file_path, minio: Minio, publish: PublishMethod):
    # Do something with the received job with file
    image = Image.open(localFilePath)
    variableToGet = someFunctionThatDoesSomethingTo(image)
    image.close()

    # Publish the result using the publish method, which is already
    # thread-safe and uses the correct AMQP channel and exchange
    publish("stringResult", variableToGet, success=True, confidence=100)


if __name__ == '__main__':
    try:
        const numberOfConcurrentJobs = 1
        worker = kimiworker.Worker(job_handler, "name-of-the-queue-for-the-worker")
        worker.start(numberOfConcurrentJobs)

    except KeyboardInterrupt:
        print('Got interruption signal. Exiting...\n')
        try:
            sys.exit(0)
        except SystemExit:
            os._exit(0)
```

### The job handler method

The job handler is called with the following paramters;

- **`log`**: The logger class which you can use to log. Logs to the console, with the correct prefix and timestamp. Methods: `info`, `warn`, `error`.
- **`job`**: The job information served by the station
- **`job_id`**: The auto-generated jobId that this worker has attached to the job
- **`local_file_path`**: The file attached to the job, already downloaded to local drive
- **`minio`**: An already connected instance to the shared MinIO storage
- **`publish`**: The callback method for publishing results back to the station. Arguments:
  - **`result`** `<Any>`: The result to send back to the station. May be an error message if `success` is False
  - **`success`** `<Boolean>`: Whether the job was succesful
  - **`confidence`** `<Int>`: The confidence-level (0-100) of the result this worker got.

## Required environment variables

### AMQP

- **`AMQP_HOST`** - The host of the station AMQP (ie 127.0.0.1)
- **`AMQP_PORT`** - The port for the station AMQP (ie 5672)
- **`AMQP_USER`** - The username for AMQP
- **`AMQP_PASS`** - The password for AMQP
- **`EXCHANGE`** - The exchange prefix to use for jobs and results (ie pageattribute-text-worker)

### MinIO

- **`MINIO_HOST`** - The host of the station MinIO (ie 127.0.0.1)
- **`MINIO_PORT`** - The port for the station MinIO (ie 9000)
- **`MINIO_ACCESS_KEY`** - The access key/username for MinIO
- **`MINIO_SECRET_KEY`** - The secret key/password for MinIO
