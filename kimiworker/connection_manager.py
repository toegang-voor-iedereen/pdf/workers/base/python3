from time import sleep
from typing import Callable, Optional

import pika
import pika.exceptions
from pika.adapters.blocking_connection import BlockingChannel

from kimiworker.config import AMQPConfig
from kimiworker.kimi_logger import KimiLogger


class ConnectionManager:
    """
    Manage AMQP connections with automatic retries and error handling.

    This class provides functionality to establish, maintain, and close AMQP connections.
    It handles automatic reconnection with configurable retry logic and provides methods
    to acquire channels and check connection status.
    """

    def __init__(
        self,
        amqp_config: AMQPConfig,
        client_properties: dict,
        retry_attempts: int = 5,
        retry_delay: Callable[[int], float] = lambda x: 2**x,
        sleep_fn: Callable[[float], None] = sleep,
        connection_factory: Callable[
            [dict, AMQPConfig], pika.BlockingConnection
        ] = lambda props, config: (
            pika.BlockingConnection(
                pika.ConnectionParameters(
                    host=config.host,
                    port=config.port,
                    credentials=pika.PlainCredentials(config.username, config.password),
                    client_properties=props,
                    heartbeat=config.heartbeat_interval,
                )
            )
        ),
        logger: Optional[KimiLogger] = None,
    ):
        """
        Initialize a ConnectionManager instance with the specified configuration.

        Args:
            amqp_config: Configuration parameters for the AMQP connection.
            client_properties: Properties to identify the client to the AMQP server.
            retry_attempts: Maximum number of connection retry attempts.
            retry_delay: Function that calculates delay between retries based on the attempt number.
            sleep_fn: Function to use for sleeping between retry attempts.
            connection_factory: Factory function that creates a new connection.
            logger: Optional logger for recording connection events.

        Note:
            The connection is established during initialization by calling _connect().
        """
        self.amqp_config = amqp_config
        self.client_properties = client_properties
        self.retry_attempts = retry_attempts
        self.retry_delay = retry_delay
        self.sleep_fn = sleep_fn
        self.connection_factory = connection_factory
        self.connection: Optional[pika.BlockingConnection] = None
        self._logger: Optional[KimiLogger] = None
        if logger:
            self.set_logger(logger)
        self._connect()

    def set_logger(self, logger: KimiLogger):
        """
        Set the logger for the ConnectionManager.

        Args:
            logger: The parent logger to use for creating a child logger.
        """
        self._logger = logger.child({"component": "ConnectionManager"})

    def _connect(self):
        """
        Establish a connection to the AMQP server with retries.

        Attempts to connect to the AMQP server up to the configured number of retry attempts.
        Between attempts, waits for a duration determined by the retry_delay function.
        Raises AMQPError if all connection attempts fail.
        """
        for attempt in range(self.retry_attempts):
            try:
                if self._logger:
                    self._logger.info(
                        f"AMQP: Connecting to server... (attempt {attempt + 1}/{self.retry_attempts})"
                    )
                self.connection = self.connection_factory(
                    self.client_properties, self.amqp_config
                )
                if self._logger:
                    self._logger.info("AMQP: Successfully connected to server.")
                return
            except pika.exceptions.AMQPError as e:
                if self._logger:
                    self._logger.error(f"AMQP: connection error: {e}")
                if attempt == self.retry_attempts - 1:
                    raise
                delay = self.retry_delay(attempt)
                if self._logger:
                    self._logger.info(f"AMQP: Retrying in {delay} seconds...")
                self.sleep_fn(delay)

    def get_channel(self) -> BlockingChannel:
        """
        Get a channel from the current connection or establish a new connection if needed.

        Returns:
            A new channel from the connection.

        Raises:
            pika.exceptions.AMQPConnectionError: If unable to establish a connection after retries.
        """
        if self.connection is None or self.connection.is_closed:
            self._connect()
        if self.connection is None:
            raise pika.exceptions.AMQPConnectionError(
                "AMQP: Failed to establish a connection after multiple retries."
            )
        try:
            return self.connection.channel()
        except pika.exceptions.AMQPError as e:
            if self._logger:
                self._logger.error(f"AMQP: Channel error {e}, attempting to reconnect")
            self._connect()
            return self.connection.channel()

    def close(self):
        """
        Close the current connection if it exists and is open.

        Logs any errors that occur during the closing process but does not raise them.
        Sets the connection to None after closing.
        """
        if self.connection and self.connection.is_open:
            if self._logger:
                self._logger.info("AMQP: Closing connection...")
            try:
                self.connection.close()
            except pika.exceptions.AMQPError as e:
                if self._logger:
                    self._logger.warning(f"AMQP: Error while closing connection: {e}")
            self.connection = None
            if self._logger:
                self._logger.info("AMQP: connection closed.")

    def is_connected(self) -> bool:
        """
        Check if the connection is established and open.

        Returns:
            True if the connection exists and is open, False otherwise.
        """
        return self.connection is not None and self.connection.is_open
