import pytest

from kimiworker.utils.content_definition import (
    find_consecutive_element_indices_with_label,
    get_highest_positioned_content,
    has_label,
    merge_labels,
)
from kimiworker.types.objects import ContentDefinition


def test_get_highest_positioned_content_with_empty_list():
    """Test get_highest_positioned_content with an empty list."""
    empty_list: list[ContentDefinition] = []

    with pytest.raises(AssertionError):
        get_highest_positioned_content(empty_list)


def test_get_highest_positioned_content_with_single_item():
    """Test get_highest_positioned_content with a single item."""

    single_item: ContentDefinition = {
        "classification": "text",
        "confidence": 0.9,
        "bbox": {"top": 0.1, "left": 0.1, "right": 0.2, "bottom": 0.2},
        "attributes": {"font_size": 12, "color": "black"},
        "labels": [{"name": "paragraph", "confidence": 0.8}],
        "children": None,
    }
    single_item_list = [single_item]

    result = get_highest_positioned_content(single_item_list)

    assert result == single_item


def test_get_highest_positioned_content_with_multiple_items():
    """Test get_highest_positioned_content with multiple items."""

    content_list: list[ContentDefinition] = [
        {
            "classification": "text",
            "confidence": 0.9,
            "bbox": {"top": 0.3, "left": 0.1, "right": 0.2, "bottom": 0.4},
            "attributes": {"font_size": 12, "color": "black"},
            "labels": [{"name": "paragraph", "confidence": 0.8}],
            "children": None,
        },
        {
            "classification": "text",
            "confidence": 0.95,
            "bbox": {"top": 0.2, "left": 0.3, "right": 0.4, "bottom": 0.4},
            "attributes": {"font_size": 16, "color": "purple"},
            "labels": None,
            "children": None,
        },
        {
            "classification": "text",
            "confidence": 0.85,
            "bbox": {"top": 0.1, "left": 0.2, "right": 0.3, "bottom": 0.3},
            "attributes": {"font_size": 16, "color": "blue"},
            "labels": [{"name": "heading", "confidence": 0.9}],
            "children": None,
        },
    ]

    result = get_highest_positioned_content(content_list)

    assert (
        result == content_list[2]
    ), "Expected the content with the minimum top position"


content: list[ContentDefinition] = [
    {
        "classification": "",
        "confidence": 100,
        "bbox": {"top": 0, "left": 0, "bottom": 0, "right": 0},
        "attributes": {},
        "labels": [{"confidence": 90, "name": "foo"}],
        "children": [],
    },
    {
        "classification": "",
        "confidence": 100,
        "bbox": {"top": 0, "left": 0, "bottom": 0, "right": 0},
        "attributes": {},
        "labels": [{"confidence": 100, "name": "bar"}],
        "children": [],
    },
    {
        "classification": "",
        "confidence": 100,
        "bbox": {"top": 0, "left": 0, "bottom": 0, "right": 0},
        "attributes": {},
        "labels": [{"confidence": 100, "name": "foo"}],
        "children": [],
    },
    {
        "classification": "",
        "confidence": 100,
        "bbox": {"top": 0, "left": 0, "bottom": 0, "right": 0},
        "attributes": {},
        "labels": [{"confidence": 90, "name": "foo"}],
        "children": [],
    },
]


def test_merge_labels():
    """Test if labels can be merged"""
    merged_labels = merge_labels(
        content[0].get("labels") or [], [content[1].get("labels") or []]
    )

    assert len(merged_labels) == 2
    assert (
        len(
            [
                label
                for label in merged_labels
                if label.get("name") == "foo" and label.get("confidence") == 90
            ]
        )
        == 1
    )

    merged_labels = merge_labels([], [item.get("labels") or [] for item in content])

    assert len(merged_labels) == 2
    assert (
        len(
            [
                label
                for label in merged_labels
                if label.get("name") == "foo" and label.get("confidence") == 100
            ]
        )
        == 1
    )


def test_has_label():
    """Test if a specific label name can be detected"""
    assert has_label(content[0], "foo") is True
    assert has_label(content[0], "bar") is False


def test_find_consecutive_element_indices_with_label():
    """Test if ranges of consecutive labels can be detected"""
    consecutive_labels = find_consecutive_element_indices_with_label(content, ["foo"])
    assert consecutive_labels == [[0], [2, 3]]

    consecutive_labels = find_consecutive_element_indices_with_label(content, ["bar"])
    assert consecutive_labels == [[1]]
