def merge_dicts(dict_a: dict, dict_b: dict) -> dict:
    """
    Merges two dictionaries, combining nested dictionaries recursively.

    If a key exists in both dictionaries:
      - and both values are dictionaries, it merges them recursively.
      - otherwise, the value from dict_b will overwrite the value from dict_a.

    Parameters:
    dict_a (dict): The first dictionary to merge.
    dict_b (dict): The second dictionary whose values will be merged into dict_a.

    Returns:
    dict: A new dictionary that is the result of merging dict_a and dict_b.
    """

    # Return the other dictionary if one of them is empty
    if not dict_a and not dict_b:
        return {}

    if dict_a is not None and not isinstance(dict_a, dict):
        raise TypeError("Dict a should be of type dictionary")
    if dict_b is not None and not isinstance(dict_b, dict):
        raise TypeError("Dict b Should be of type dictionary")
    if not dict_a:
        return dict_b.copy()
    if not dict_b:
        return dict_a.copy()

    # Create a copy of dict_a to avoid mutating the input
    merged_dict = dict(dict_a)

    # Iterate through each key in dict_b
    for key, value in dict_b.items():
        # Check if the key is in dict_a and both values are dictionaries
        if key in dict_a and isinstance(dict_a[key], dict) and isinstance(value, dict):
            # Recursively merge the nested dictionaries
            merged_dict[key] = merge_dicts(dict_a[key], value)
        else:
            # Otherwise, overwrite the value from dict_a with dict_b's value
            merged_dict[key] = value

    return merged_dict
