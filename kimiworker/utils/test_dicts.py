import pytest

from kimiworker.utils.dicts import merge_dicts


def test_basic_merge():
    dict_a = {"a": 1, "b": 2}
    dict_b = {"c": 3, "d": 4}
    expected = {"a": 1, "b": 2, "c": 3, "d": 4}
    assert merge_dicts(dict_a, dict_b) == expected


def test_override_value():
    dict_a = {"a": 1, "b": 2}
    dict_b = {"b": 3, "c": 4}
    expected = {"a": 1, "b": 3, "c": 4}
    assert merge_dicts(dict_a, dict_b) == expected


def test_recursive_merge():
    dict_a = {"a": {"x": 1}, "b": 2}
    dict_b = {"a": {"y": 2}, "c": 3}
    expected = {"a": {"x": 1, "y": 2}, "b": 2, "c": 3}
    assert merge_dicts(dict_a, dict_b) == expected


def test_recursive_override():
    dict_a = {"a": {"x": 1}, "b": 2}
    dict_b = {"a": {"x": 2, "y": 2}, "c": 3}
    expected = {"a": {"x": 2, "y": 2}, "b": 2, "c": 3}
    assert merge_dicts(dict_a, dict_b) == expected


def test_empty_first_dict():
    dict_a = {}
    dict_b = {"a": 1, "b": 2}
    expected = {"a": 1, "b": 2}
    assert merge_dicts(dict_a, dict_b) == expected


def test_empty_second_dict():
    dict_a = {"a": 1, "b": 2}
    dict_b = {}
    expected = {"a": 1, "b": 2}
    assert merge_dicts(dict_a, dict_b) == expected


def test_both_empty_dicts():
    dict_a = {}
    dict_b = {}
    expected = {}
    assert merge_dicts(dict_a, dict_b) == expected


def test_non_dict_value():
    dict_a = {"a": 1, "b": 2}
    dict_b = {"a": "overwrite", "b": [1, 2, 3]}
    expected = {"a": "overwrite", "b": [1, 2, 3]}
    assert merge_dicts(dict_a, dict_b) == expected


# This test checks what happens if one value is a dict and the other is not
def test_mixed_type_value():
    dict_a = {"a": {"x": 1}}
    dict_b = {"a": 2}
    expected = {"a": 2}
    assert merge_dicts(dict_a, dict_b) == expected


@pytest.mark.parametrize("invalid_dict", [123, "string", [1, 2, 3]])
def test_invalid_input_types(invalid_dict):
    dict_a = {"a": 1}
    with pytest.raises(TypeError):
        merge_dicts(dict_a, invalid_dict)
