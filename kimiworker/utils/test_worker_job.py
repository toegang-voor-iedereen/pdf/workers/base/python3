from kimiworker.types.worker_job import WorkerJob
from kimiworker.utils.worker_job import extract_best_attribute_value


def test_extract_best_attribute_value_with_matching_job_id():
    """
    Tests extract_best_attribute_value function to ensure it correctly finds and returns
    the dictionary with a matching job ID.
    """

    job: WorkerJob = {
        "recordId": "x",
        "bucketName": "x",
        "filename": "x",
        "attributes": {
            "interpretedContent": {
                "bestJobId": "123",
                "values": [
                    {"jobId": "122", "value": "A"},
                    {"jobId": "123", "value": "B"},
                    {"jobId": "124", "value": "C"},
                ],
            },
        },
    }

    expected = {"jobId": "123", "value": "B"}
    assert (
        extract_best_attribute_value(job, "interpretedContent") == expected
    ), "Should return the matching job ID dictionary"


def test_extract_best_attribute_value_no_matching_job_id():
    """
    Tests extract_best_attribute_value to verify that it returns None when no job ID matches.
    """
    job: WorkerJob = {
        "recordId": "x",
        "bucketName": "x",
        "filename": "x",
        "attributes": {
            "interpretedContent": {
                "bestJobId": "125",
                "values": [
                    {"jobId": "122", "value": "A"},
                    {"jobId": "123", "value": "B"},
                    {"jobId": "124", "value": "C"},
                ],
            },
        },
    }
    assert (
        extract_best_attribute_value(job, "interpretedContent") is None
    ), "Should return None for no matching job ID"


def test_extract_best_attribute_value_empty_values():
    """
    Tests extract_best_attribute_value to ensure it handles empty values list correctly.
    """
    job: WorkerJob = {
        "recordId": "x",
        "bucketName": "x",
        "filename": "x",
        "attributes": {
            "interpretedContent": {
                "bestJobId": "125",
                "values": [],
            },
        },
    }
    assert (
        extract_best_attribute_value(job, "interpretedContent") is None
    ), "Should return None when values list is empty"


def test_extract_best_attribute_value_missing_keys():
    """
    Tests extract_best_attribute_value to ensure it correctly handles missing keys.
    """
    job: WorkerJob = {
        "recordId": "x",
        "bucketName": "x",
        "filename": "x",
        "attributes": {
            "interpretedContent": {
                "values": [],
            },
        },
    }
    assert (
        extract_best_attribute_value(job, "interpretedContent") is None
    ), "Should return None when key is missing"


def test_extract_best_attribute_value_incorrect_type():
    """
    Tests extract_best_attribute_value to ensure it returns None when the jobId in values does not match the expected type.
    """
    job: WorkerJob = {
        "recordId": "x",
        "bucketName": "x",
        "filename": "x",
        "attributes": {
            "interpretedContent": {
                "bestJobId": "123",
                "values": [
                    {
                        "jobId": 123,
                        "value": "Incorrect Type",
                    },  # This should not be matched due to type mismatch
                    {"jobId": "123", "value": "Correct Type"},
                ],
            },
        },
    }
    expected = {"jobId": "123", "value": "Correct Type"}
    assert (
        extract_best_attribute_value(job, "interpretedContent") == expected
    ), "Should return the correct type match"
