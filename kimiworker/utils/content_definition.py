"""
Module for working with content definitions.

This module provides utility functions for working with content definitions,
which represent elements or objects within a document or page. It includes
a function to retrieve the content that is positioned highest on the page.
"""

from itertools import chain, groupby
from operator import itemgetter
from typing import List
from kimiworker.types.objects import ContentDefinition, ContentLabel


def get_highest_positioned_content(
    content: list[ContentDefinition],
) -> ContentDefinition:
    """
    Get the content that is positioned highest on the page.

    Args:
        content (list[ContentDefinition]): A list of content definitions.

    Returns:
        ContentDefinition: The content that is positioned highest on the page.
    """
    # Ensure that the list of content is not empty
    assert len(content) > 0, "Content list must not be empty"

    # Find the content with the minimum top position
    highest_positioned_content = min(content, key=lambda x: x.get("bbox").get("top"))

    return highest_positioned_content


def has_label(element: ContentDefinition, label: str):
    """Check if an element has a label"""
    return any(
        content_label["name"] == label for content_label in element.get("labels") or []
    )


def find_consecutive_element_indices_with_label(
    elements: List[ContentDefinition], labels: List[str], exclude_labels: List[str] = []
) -> list[list[int]]:
    """
    Identify and return lists of consecutive indices in 'elements' where each element
    has a specified 'label' and lacks any of the 'exclude_labels'.

    This function iterates over the given list of 'elements', checking for the presence
    of a specific 'label' and the absence of all labels listed in 'exclude_labels'.
    It then groups and returns the indices of these elements when they occur consecutively.

    Args:
        - elements (List[ContentDefinition]): A list of content definitions to be examined.
        - labels (List[str]): A list of labels that must be present in the element for it to be included.
        - exclude_labels (List[str]): A list of labels that should not be present in the element.

    Returns:
        - list[list[int]]: A list of lists, where each sublist contains consecutive indices
        of elements that meet the inclusion and exclusion criteria.
    """
    indices = [
        i
        for i, element in enumerate(elements)
        if all(has_label(element, label) for label in labels)
        and not any(has_label(element, ex_label) for ex_label in exclude_labels)
    ]
    return [
        list(map(itemgetter(1), g))
        for k, g in groupby(enumerate(indices), lambda x: x[0] - x[1])
    ]


def merge_labels(current: list[ContentLabel], merge: list[list[ContentLabel]]):
    """Merges into current labels if label not present or if it has a higher confidence value"""
    labels_dict = {
        current_label.get("name"): current_label for current_label in current
    }

    for label in chain.from_iterable(merge):
        name = label.get("name")
        confidence = label.get("confidence")
        if name not in labels_dict or confidence > labels_dict[name].get("confidence"):
            labels_dict[name] = label

    return list(labels_dict.values())


def get_content_definition_float_attribute(
    content: ContentDefinition, attribute: str
) -> float:
    """
    Extracts the float value of a specified attribute from a content definition.

    Args:
        content (ContentDefinition): The content definition from which to extract the attribute.
        attribute (str): The name of the attribute to extract.

    Returns:
        float: The float value of the attribute, or 0.0 if the attribute does not exist.
    """
    return float(content.get("attributes", {}).get(attribute, 0))
