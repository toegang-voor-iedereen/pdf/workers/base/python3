"""
WorkerJob utility functions
"""

from typing import Optional
from pathdict import PathDict

from kimiworker.types.worker_job import WorkerJob


def extract_best_attribute_value(job: WorkerJob, attribute: str) -> Optional[dict]:
    """
    Extracts the best value from a WorkerJob attribute based on the best job ID.

    This function attempts to retrieve a specific attribute from a WorkerJob, identifies the best
    job ID for this attribute, and then finds the corresponding value that matches this job ID.

    This functionality should become part of a separate library at some point

    Args:
        job (WorkerJob): A WorkerJob.
        attribute (str): The attribute key whose best value is to be extracted.

    Returns:
        Optional[dict]: The dictionary containing the best attribute value or None if no matching value is found.

    Raises:
        None explicitly, but will return None if the necessary keys are not present or the data is malformed.
    """
    try:
        attributes = PathDict(job.get("attributes", {}))
        best_job_id = attributes[f"{attribute}.bestJobId"]
        values = attributes[f"{attribute}.values"]
        return next((x for x in values or [] if x["jobId"] == best_job_id), None)
    except KeyError:
        return None
