from __future__ import annotations

"""
Module for converting bounding boxes.

This module provides functions for converting bounding boxes between different formats.
It includes functions to convert pixel-based bounding boxes to percentage-based bounding boxes,
and to convert bounding boxes to Shapely Polygon objects.
"""

from functools import lru_cache
from typing import Dict
from shapely.geometry import Polygon
from kimiworker.types.objects import BoundingBox, ContentDefinition


@lru_cache
def convert_bounding_box(
    left: float,
    top: float,
    right: float,
    bottom: float,
    image_width: int,
    image_height: int,
) -> BoundingBox:
    """
    Convert pixel-based bounding box coordinates to percentage-based bounding box.

    Args:
        left (float): The left coordinate of the bounding box.
        top (float): The top coordinate of the bounding box.
        right (float): The right coordinate of the bounding box.
        bottom (float): The bottom coordinate of the bounding box.
        image_width (int): The width of the image.
        image_height (int): The height of the image.

    Returns:
        BoundingBox: A dictionary representing the converted bounding box with
                     percentage-based coordinates.
    """
    # Calculate percentage-based coordinates for the bounding box
    return {
        "top": top / image_height,
        "left": left / image_width,
        "bottom": bottom / image_height,
        "right": right / image_width,
    }


def bounding_box_to_dict(bounding_box: BoundingBox) -> Dict[str, float]:
    """
    Convert bounding box object to straight up Dict[str, float] as a TypedDict
    does not conform to Mapping Dict[str, float] even though it should.
    -- https://stackoverflow.com/questions/71986632/automatic-upcast-of-typeddict-to-dictstr-any

    Args:
        bounding_box (BoundingBox): The bounding box to convert to a dict

    Returns:
        Dict[str, float]: the values of the bounding box presented as a regular Dict
    """
    return {
        "top": bounding_box.get("top"),
        "left": bounding_box.get("left"),
        "right": bounding_box.get("right"),
        "bottom": bounding_box.get("bottom"),
    }


def bounding_box_to_polygon(bbox: BoundingBox) -> Polygon:
    """
    Convert a BoundingBox object to a Shapely Polygon.

    Args:
        bbox (BoundingBox): A dictionary representing the bounding box with
                            percentage-based coordinates.

    Returns:
        Polygon: A Shapely Polygon representing the bounding box.
    """
    # Create a Shapely Polygon from the bounding box coordinates
    return Polygon.from_bounds(
        bbox.get("left"), bbox.get("top"), bbox.get("right"), bbox.get("bottom")
    )


def border_polygons(bbox: BoundingBox, border_size: float) -> tuple[Polygon, Polygon]:
    """
    Generate polygons for the top and bottom borders of a given bounding box.

    This function calculates the top and bottom border polygons by expanding the bounding box
    upwards and downwards respectively by a specified border size.

    Parameters:
        - bbox (BoundingBox): A dictionary representing the bounding box with keys 'top', 'bottom',
        'left', and 'right', defining the coordinates of the box.
        - border_size (float): The size of the border to add on the top and bottom of the bounding box.

    Returns:
        - Tuple[Polygon]: A tuple containing shapely Polygons,
            each representing a polygon for the top and bottom borders.
    """
    # Calculate the top border polygon by moving the top boundary up and setting the
    # bottom boundary to the original top.
    top_border_box_polygon = bounding_box_to_polygon(
        {
            **bbox,
            "top": bbox.get("top") - border_size,
            "bottom": bbox.get("top"),
        }
    )

    # Calculate the bottom border polygon by setting the top boundary to the original
    # bottom and moving the bottom boundary down.
    bottom_border_box_polygon = bounding_box_to_polygon(
        {
            **bbox,
            "top": bbox.get("top"),
            "bottom": bbox.get("top") + border_size,
        }
    )

    return top_border_box_polygon, bottom_border_box_polygon


def get_enclosing_bbox(resources: list[AnyResource]) -> BoundingBox:  # type: ignore - for circular reference to Resource
    """
    Calculate the minimum enclosing bounding box that contains all the bounding boxes
    in the given list of elements.

    Each element in the list should be a dictionary representing a `ContentDefinition`,
    which includes a `bbox` key with another dictionary defining the 'top', 'left', 'right',
    and 'bottom' coordinates of the bounding box as floats between 0 and 1.

    Parameters:
        elements (list[ContentDefinition]): A list of dictionaries, each containing a `bbox`
            dictionary with 'top', 'left', 'right', and 'bottom' coordinates.

    Returns:
        BoundingBox: A dictionary with keys 'top', 'left', 'right', and 'bottom' representing
        the coordinates of the calculated enclosing bounding box. These coordinates are
        computed as the minimum top, minimum left, maximum right, and maximum bottom values
        from the bounding boxes of the provided elements.
    """
    return {
        "top": min(
            resource.certain_bounding_box().get("top") for resource in resources
        ),
        "left": min(
            resource.certain_bounding_box().get("left") for resource in resources
        ),
        "right": max(
            resource.certain_bounding_box().get("right") for resource in resources
        ),
        "bottom": max(
            resource.certain_bounding_box().get("bottom") for resource in resources
        ),
    }


def get_enclosing_bbox(elements: list[ContentDefinition]) -> BoundingBox:
    """
    Calculate the minimum enclosing bounding box that contains all the bounding boxes
    in the given list of elements.

    Each element in the list should be a dictionary representing a `ContentDefinition`,
    which includes a `bbox` key with another dictionary defining the 'top', 'left', 'right',
    and 'bottom' coordinates of the bounding box as floats between 0 and 1.

    Parameters:
        elements (list[ContentDefinition]): A list of dictionaries, each containing a `bbox`
            dictionary with 'top', 'left', 'right', and 'bottom' coordinates.

    Returns:
        BoundingBox: A dictionary with keys 'top', 'left', 'right', and 'bottom' representing
        the coordinates of the calculated enclosing bounding box. These coordinates are
        computed as the minimum top, minimum left, maximum right, and maximum bottom values
        from the bounding boxes of the provided elements.
    """
    return {
        "top": min(element.get("bbox").get("top") for element in elements),
        "left": min(element.get("bbox").get("left") for element in elements),
        "right": max(element.get("bbox").get("right") for element in elements),
        "bottom": max(element.get("bbox").get("bottom") for element in elements),
    }
