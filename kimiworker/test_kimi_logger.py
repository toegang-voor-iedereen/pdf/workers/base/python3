import logging
from unittest.mock import MagicMock

from kimiworker.kimi_logger import KimiLogger


def test_logger_initialization():
    logger = KimiLogger(data={"initial": "data"}, logger_name="test_logger")
    assert logger.data == {"initial": "data"}
    assert logger.logger_name == "test_logger"
    assert logger.logger.level == logging.DEBUG


def test_child_logger_inherits_data():
    parent_logger = KimiLogger(data={"user": "admin"}, logger_name="test_logger")
    child_logger = parent_logger.child({"session": "xyz"})
    assert child_logger.logger_name == "test_logger"

    assert child_logger.data == {"user": "admin", "session": "xyz"}
    assert child_logger.logger_name == parent_logger.logger_name


def test_logger_methods():
    logger = KimiLogger()
    logger.logger = MagicMock()  # Mock the logger to intercept log calls

    logger.debug("Test debug")
    logger.info("Test info")
    logger.warning("Test warning")
    logger.error("Test error")
    logger.critical("Test critical")

    logger.logger.debug.assert_called_once_with("Test debug", extra={})
    logger.logger.info.assert_called_once_with("Test info", extra={})
    logger.logger.warning.assert_called_once_with("Test warning", extra={})
    logger.logger.error.assert_called_once_with("Test error", extra={})
    logger.logger.critical.assert_called_once_with("Test critical", extra={})


def test_event_logging():
    logger = KimiLogger()
    logger.child = MagicMock(
        return_value=logger
    )  # Mock child to return self for chaining
    logger.info = MagicMock()  # Mock info to test event method indirectly

    logger.event("login_attempt", {"user": "testuser"})

    expected_data = {"kimiEvent": {"name": "login_attempt", "user": "testuser"}}
    logger.child.assert_called_once_with(expected_data)
    logger.info.assert_called_once_with("kimiEvent.login_attempt")
