"""
This module provides the KimiLogger class for easy logging with ECS (Elastic Common Schema) format.

It utilizes the ecs_logging library to format log messages according to ECS, allowing
for structured and query-friendly logging, especially useful in containerized environments.
"""

import logging
import sys

import ecs_logging

from kimiworker.utils.dicts import merge_dicts

# Configure the logging output for stdout with ECS formatting
ecs_formatter = ecs_logging.StdlibFormatter()
stdout_stream = logging.StreamHandler(sys.stdout)
stdout_stream.setFormatter(ecs_formatter)


class KimiLogger:
    """
    Custom logger that outputs in ECS format and supports hierarchical logging by merging
    dictionaries to accumulate context.

    Attributes:
        data (dict): Stores the initial logging context.
        logger_name (str): The name of the logger.
        logger (logging.Logger): The configured logger instance.
    """

    def __init__(self, data=None, logger_name="logger"):
        """
        Initialize a KimiLogger instance with optional context data and a logger name.

        Args:
            data (dict, optional): Initial context data for the logger. Defaults to None.
            logger_name (str, optional): Name of the logger. Defaults to "logger".
        """
        if data is None:
            data = {}  # Use a mutable default parameter safely
        self.data = data
        self.logger_name = logger_name
        self.logger = logging.getLogger(logger_name)
        self.logger.setLevel(logging.DEBUG)
        self.logger.addHandler(stdout_stream)

    def child(self, data: dict):
        """
        Create a child logger with additional data merged into the current context.

        Args:
            data (dict): Additional context data to merge.

        Returns:
            KimiLogger: A new instance of KimiLogger with extended context.
        """
        return KimiLogger(merge_dicts(self.data, data), logger_name=self.logger_name)

    def event(self, event_name: str, parameters=None):
        """
        Log an event with a specified name and parameters.

        Args:
            event_name (str): The name of the event to log.
            parameters (dict, optional): Additional parameters for the event. Defaults to None.
        """
        if parameters is None:
            parameters = {}
        loggerData = merge_dicts({"name": event_name}, parameters)
        self.child({"kimiEvent": loggerData}).info("kimiEvent." + event_name)

    def debug(self, message: str):
        """Log a debug message."""
        self.logger.debug(message, extra=self.data)

    def trace(self, message: str):
        """Log a trace message, aliased to debug in this logger setup."""
        self.logger.debug(message, extra=self.data)

    def info(self, message: str):
        """Log an informational message."""
        self.logger.info(message, extra=self.data)

    def warning(self, message: str):
        """Log a warning message."""
        self.logger.warning(message, extra=self.data)

    def error(self, message: str):
        """Log an error message."""
        self.logger.error(message, extra=self.data)

    def critical(self, message: str):
        """Log a critical message."""
        self.logger.critical(message, extra=self.data)
