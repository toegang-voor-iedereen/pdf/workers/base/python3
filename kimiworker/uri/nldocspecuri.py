def nldocspec_uri(subject: str):
    return f"https://spec.nldoc.nl/{subject}"


def nldocspec_resource_uri(subject: str):
    return nldocspec_uri(f"Resource/{subject}")


def nldocspec_property_uri(subject: str):
    return nldocspec_uri(f"Property/{subject}")


def kimi_uri(subject: str):
    return f"https://kimi.spec.nldoc.nl/{subject}"


def kimi_label_uri(label: str):
    return kimi_uri(f"Label/{label}")


def kimi_attribute_uri(label: str):
    return kimi_uri(f"Attribute/{label}")
