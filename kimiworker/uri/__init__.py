from .nldocspecuri import (
    nldocspec_property_uri,
    nldocspec_resource_uri,
    kimi_uri,
    kimi_label_uri,
    kimi_attribute_uri,
)
