#!/usr/bin/env python
import time
import uuid

import pika
import pika.exceptions
from minio import Minio
from minio.error import S3Error

from .config import Config
from .job_listener import JobListener
from .kimi_logger import KimiLogger
from .types.job_handler import JobHandler
from .version import version as kimi_baseworker_version

instance_trace_id = str(uuid.uuid4())
kimi_logger = KimiLogger(
    {
        "trace": {"id": instance_trace_id},
    },
    logger_name="kimi-worker",
)


class Worker:
    """
    The Worker class encapsulates functionality to process jobs received from a message queue,
    using a specified job handler, and interacts with a MinIO server for related data storage.
    """

    def __init__(self, job_handler: JobHandler, queue_name: str, auto_download=False):
        """
        Initialize the Worker with the required job handler and queue configurations.
        Environment variables are used to setup the connection to MinIO.

        Args:
            job_handler (JobHandler): The handler responsible for processing jobs.
            queue_name (str): The name of the queue from which jobs are received.
            auto_download (bool, optional): Whether to automatically download job data. Defaults to False.
        """

        self.config = Config()

        self.minio = Minio(
            endpoint=f"{self.config.minio.host}:{self.config.minio.port}",
            access_key=self.config.minio.access_key,
            secret_key=self.config.minio.secret_key,
            secure=self.config.minio.use_ssl,
            cert_check=False,
        )

        self.logger = kimi_logger.child(
            {
                "service": {"name": queue_name, "version": kimi_baseworker_version},
                "worker": {
                    "name": queue_name,
                    "type": self.config.amqp.exchange,
                    "instanceName": (
                        self.config.hostname
                        if self.config.hostname != ""
                        else f"unknown-worker-instance-name-{instance_trace_id}"
                    ),
                    "baseWorkerVersion": kimi_baseworker_version,
                },
            }
        )

        self.job_handler = job_handler
        self.job_listener = JobListener(
            logger=self.logger,
            minio=self.minio,
            job_handler=self.job_handler,
            queue_name=queue_name,
            auto_download=auto_download,
            amqp_config=self.config.amqp,
            hostname=self.config.hostname,
        )

    def start(self, number_of_concurrent_jobs=1):
        """
        Start the Worker processes including MinIO connection assertion and job listener start.

        Args:
            number_of_concurrent_jobs (int, optional): The number of concurrent jobs to process. Defaults to 1.
        """
        self.__start_minio(logger=self.logger.child({"phase": "MinIO"}))
        self.__start_job_listener(
            logger=self.logger.child({"phase": "Listener"}),
            number_of_concurrent_jobs=number_of_concurrent_jobs,
        )

    def __start_minio(self, logger: KimiLogger):
        """
        Internal method to assert and establish MinIO connection.
        Exits if connection cannot be established after maximum retries.

        Args:
            logger (KimiLogger): Logger instance for logging information.
        """
        logger.info("Asserting MinIO connection...")
        retry_count = 0
        max_retries = 10

        while retry_count < max_retries:
            try:
                files_bucket_exists = self.minio.bucket_exists("files")
                if not files_bucket_exists:
                    logger.error("Files bucket does not exist. Exiting...")
                    exit(1)

            except S3Error as error:
                if error.code == "SignatureDoesNotMatch":
                    logger.error("Invalid MinIO credentials")
                    exit(1)

                if error.code == "InvalidAccessKeyId":
                    logger.warning("Access keys not present yet.")
                    time.sleep(1.0)
                    retry_count += 1
                    logger.info(f"Retrying MinIO (Attempt #{retry_count + 1})")
                    continue

                logger.error(f"S3 Error: {error}")
                exit(1)

            except Exception as e:
                wait_time = 5.0 + retry_count
                retry_count += 1  # Increment retry count for general exceptions

                if retry_count >= max_retries:
                    logger.error(
                        f"Failed to connect to MinIO after {max_retries} attempts. Exiting..."
                    )
                    exit(1)

                if retry_count > 5:
                    logger.child({"exception": repr(e)}).warning(
                        f"Failed to confirm MinIO bucket. Waiting {wait_time} seconds before retrying..."
                    )
                else:
                    logger.child({"exception": repr(e)}).debug(
                        f"Failed to confirm MinIO bucket. Waiting {wait_time} seconds before retrying..."
                    )

                time.sleep(wait_time)
                logger.debug(f"Retrying MinIO (Attempt #{retry_count + 1})")
                continue

            else:
                logger.info("Connected with MinIO")
                break

    def __start_job_listener(
        self, logger: KimiLogger, number_of_concurrent_jobs: int = 1
    ):
        """
        Internal method to start the job listener for processing jobs from the queue.

        Args:
            logger (KimiLogger): Logger instance for logging information.
            number_of_concurrent_jobs (int): The number of jobs to process concurrently.
        """
        logger.info("Starting job listener...")
        try:
            self.job_listener.start(number_of_concurrent_jobs)
            logger.info("Job listener started successfully.")
        except pika.exceptions.AMQPConnectionError as err:
            logger.error(f"Fatal AMQP connection error: {err}. Exiting.")
            exit(1)
        except Exception as err:
            logger.error(f"Unexpected error starting job listener: {err}. Exiting.")
            exit(1)
