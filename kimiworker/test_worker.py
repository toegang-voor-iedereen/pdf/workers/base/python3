import os
from http.client import HTTPResponse
from unittest.mock import ANY, Mock, call, patch

import pytest
from minio.error import S3Error

from kimiworker.worker import Worker


def create_mock_response():
    """Create a mock HTTP response for S3Error"""
    mock_response = Mock(spec=HTTPResponse)
    mock_response.status = 403
    mock_response.reason = "Forbidden"
    mock_response.read = lambda: b""
    mock_response.getheader = lambda x: None
    return mock_response


class TestWorker:
    """Tests for the Worker class."""

    @pytest.fixture(autouse=True)
    def mock_settings_env_vars(self):
        """Set up required environment variables for all tests"""
        with patch.dict(
            os.environ,
            {
                "AMQP_HOST": "localhost",
                "AMQP_PORT": "1234",
                "AMQP_USER": "amqp-user",
                "AMQP_PASS": "amqp-pass",
                "EXCHANGE": "foo-exchange",
                "MINIO_HOST": "localhost",
                "MINIO_PORT": "1234",
                "MINIO_ACCESS_KEY": "minio-access-key",
                "MINIO_SECRET_KEY": "minio-secret-key",
                "HOSTNAME": "test-host",
            },
        ):
            yield

    @pytest.fixture
    def mock_minio_client(self, mocker):
        """Fixture to mock Minio client."""
        mock = mocker.patch("kimiworker.worker.Minio")
        return mock.return_value

    @pytest.fixture
    def worker(self, mocker, mock_minio_client):
        """Fixture to create a Worker instance with mocked dependencies."""
        job_handler_mock = Mock()

        with patch("kimiworker.connection_manager.ConnectionManager") as mock_cm:
            mock_connection = Mock()
            mock_channel = Mock()

            mock_cm.return_value.connection = mock_connection
            mock_cm.return_value.get_channel.return_value = mock_channel
            mock_cm.return_value.is_connected.return_value = True

            with patch("kimiworker.job_listener.ConnectionManager", mock_cm):
                worker_instance = Worker(
                    job_handler=job_handler_mock, queue_name="test_queue"
                )
                return worker_instance

    def test_initialization(self, worker, mock_minio_client):
        """Test Worker initialization with correct environment variables."""
        assert worker.config.minio.host == "localhost"
        assert worker.config.minio.port == "1234"
        assert worker.config.minio.access_key == "minio-access-key"
        assert worker.config.minio.secret_key == "minio-secret-key"
        assert isinstance(worker.minio, Mock)

    def test_initialization_fails_without_minio_keys(self, mocker):
        """Test Worker initialization fails if MinIO keys are missing."""
        with patch("kimiworker.connection_manager.ConnectionManager") as mock_cm:
            mock_cm.return_value.connection = Mock()
            mock_cm.return_value.get_channel.return_value = Mock()

            with patch("kimiworker.job_listener.ConnectionManager", mock_cm):
                with patch.dict(
                    os.environ,
                    {
                        "MINIO_ACCESS_KEY": "",
                        "MINIO_SECRET_KEY": "",
                    },
                ):
                    with pytest.raises(SystemExit) as excinfo:
                        Worker(Mock(), "test_queue")
                    assert excinfo.type == SystemExit
                    assert excinfo.value.code == 1

    @patch("kimiworker.worker.kimi_logger")
    def test_start(self, mock_logger, worker, mocker):
        """Test that starting the worker sets up Minio and job listener correctly."""
        mock_start_minio = mocker.patch.object(worker, "_Worker__start_minio")
        mock_start_job_listener = mocker.patch.object(
            worker, "_Worker__start_job_listener"
        )

        worker.start(3)  # Example with 3 concurrent jobs

        mock_start_minio.assert_called_once()
        mock_start_job_listener.assert_called_once_with(
            logger=ANY, number_of_concurrent_jobs=3
        )

    @patch("time.sleep")
    @patch("kimiworker.worker.kimi_logger")
    def test_start_minio_success(
        self, mock_logger, mock_sleep, worker, mock_minio_client
    ):
        """Test successful MinIO connection."""
        mock_minio_client.bucket_exists.return_value = True

        worker._Worker__start_minio(mock_logger)

        mock_minio_client.bucket_exists.assert_called_once_with("files")
        mock_logger.info.assert_has_calls(
            [call("Asserting MinIO connection..."), call("Connected with MinIO")]
        )
        mock_sleep.assert_not_called()

    @patch("time.sleep")
    @patch("kimiworker.worker.kimi_logger")
    def test_start_minio_missing_bucket(
        self, mock_logger, mock_sleep, worker, mock_minio_client
    ):
        """Test MinIO connection with missing files bucket."""
        mock_minio_client.bucket_exists.return_value = False

        with pytest.raises(SystemExit):
            worker._Worker__start_minio(mock_logger)

        mock_logger.error.assert_called_once_with(
            "Files bucket does not exist. Exiting..."
        )
        mock_sleep.assert_not_called()

    @patch("time.sleep")
    @patch("kimiworker.worker.kimi_logger")
    def test_start_minio_invalid_credentials(
        self, mock_logger, mock_sleep, worker, mock_minio_client
    ):
        """Test MinIO connection with invalid credentials."""
        mock_response = create_mock_response()
        mock_minio_client.bucket_exists.side_effect = S3Error(
            code="SignatureDoesNotMatch",
            message="Invalid credentials",
            resource="test-bucket",
            request_id="test-request",
            host_id="test-host",
            response=mock_response,
        )

        with pytest.raises(SystemExit):
            worker._Worker__start_minio(mock_logger)

        mock_logger.error.assert_called_once_with("Invalid MinIO credentials")
        mock_sleep.assert_not_called()

    @patch("time.sleep")
    @patch("kimiworker.worker.kimi_logger")
    def test_start_minio_retry_access_keys(
        self, mock_logger, mock_sleep, worker, mock_minio_client
    ):
        """Test MinIO connection retry with missing access keys."""
        mock_response = create_mock_response()
        mock_minio_client.bucket_exists.side_effect = [
            S3Error(
                code="InvalidAccessKeyId",
                message="Invalid access key",
                resource="test-bucket",
                request_id="test-request",
                host_id="test-host",
                response=mock_response,
            ),
            True,
        ]

        worker._Worker__start_minio(mock_logger)

        assert mock_minio_client.bucket_exists.call_count == 2
        mock_logger.warning.assert_called_once_with("Access keys not present yet.")
        mock_sleep.assert_called_once_with(1.0)

    @patch("time.sleep")
    @patch("kimiworker.worker.kimi_logger")
    def test_start_minio_max_retries_exceeded(
        self, mock_logger, mock_sleep, worker, mock_minio_client
    ):
        """Test MinIO connection fails after max retries."""
        mock_exception = Exception("Connection failed")
        mock_minio_client.bucket_exists.side_effect = [mock_exception] * 10

        with pytest.raises(SystemExit):
            worker._Worker__start_minio(mock_logger)

        assert mock_minio_client.bucket_exists.call_count == 10  # Max retries
        assert mock_sleep.call_count == 9  # One less than max retries
        mock_logger.error.assert_called_with(
            "Failed to connect to MinIO after 10 attempts. Exiting..."
        )

    @patch("time.sleep")
    @patch("kimiworker.worker.kimi_logger")
    def test_start_minio_general_s3_error(
        self, mock_logger, mock_sleep, worker, mock_minio_client
    ):
        """Test MinIO connection with general S3 error."""
        mock_response = create_mock_response()
        mock_error = S3Error(
            code="SomeOtherError",
            message="Some other S3 error",
            resource="test-bucket",
            request_id="test-request",
            host_id="test-host",
            response=mock_response,
        )
        mock_minio_client.bucket_exists.side_effect = mock_error

        with pytest.raises(SystemExit):
            worker._Worker__start_minio(mock_logger)

        mock_logger.error.assert_called_with(f"S3 Error: {mock_error}")
        mock_sleep.assert_not_called()
