import pytest
from unittest.mock import MagicMock
from minio import Minio
from .minio_remote_file_storage import MinioRemoteFileStorage


@pytest.fixture
def mock_minio():
    return MagicMock(spec=Minio)


@pytest.fixture
def storage(mock_minio):
    return MinioRemoteFileStorage(mock_minio)


def test_get_object_success(storage, mock_minio):
    mock_minio.get_object.return_value = MagicMock(data=b"test data")
    result = storage.get_object("test-bucket", "test-object")
    assert result == b"test data"
    mock_minio.get_object.assert_called_once_with(
        bucket_name="test-bucket",
        object_name="test-object",
        offset=0,
        length=0,
        request_headers=None,
        version_id=None,
        extra_query_params=None,
    )


def test_get_object_with_params(storage, mock_minio):
    mock_minio.get_object.return_value = MagicMock(data=b"partial data")
    result = storage.get_object(
        "test-bucket", "test-object", offset=10, length=20, version_id="v1"
    )
    assert result == b"partial data"
    mock_minio.get_object.assert_called_once_with(
        bucket_name="test-bucket",
        object_name="test-object",
        offset=10,
        length=20,
        request_headers=None,
        version_id="v1",
        extra_query_params=None,
    )


def test_fget_object_success(storage, mock_minio):
    storage.fget_object("test-bucket", "test-object", "/path/to/file")
    mock_minio.fget_object.assert_called_once_with(
        bucket_name="test-bucket",
        object_name="test-object",
        file_path="/path/to/file",
        request_headers=None,
        version_id=None,
        extra_query_params=None,
        tmp_file_path=None,
    )


def test_fput_object_success(storage, mock_minio):
    mock_result = MagicMock(
        bucket_name="test-bucket",
        object_name="test-object",
        etag="etag123",
        version_id="v1",
    )
    mock_minio.fput_object.return_value = mock_result

    result = storage.fput_object(
        "test-bucket",
        "test-object",
        "/path/to/file",
        content_type="text/plain",
        tags={"tag1": "value1"},
        legal_hold=True,
    )

    assert result == {
        "bucket_name": "test-bucket",
        "object_name": "test-object",
        "etag": "etag123",
        "version_id": "v1",
    }

    mock_minio.fput_object.assert_called_once()
    args, kwargs = mock_minio.fput_object.call_args
    assert kwargs["bucket_name"] == "test-bucket"
    assert kwargs["object_name"] == "test-object"
    assert kwargs["file_path"] == "/path/to/file"
    assert kwargs["tags"]["tag1"] == "value1"
    assert kwargs["legal_hold"] is True


def test_fput_object_with_default_params(storage, mock_minio):
    mock_result = MagicMock(
        bucket_name="default-bucket",
        object_name="default-object",
        etag="default-etag",
        version_id=None,
    )
    mock_minio.fput_object.return_value = mock_result

    result = storage.fput_object("default-bucket", "default-object", "/default/path")

    assert result["etag"] == "default-etag"
    mock_minio.fput_object.assert_called_once_with(
        bucket_name="default-bucket",
        object_name="default-object",
        file_path="/default/path",
        content_type="application/octet-stream",
        metadata=None,
        part_size=0,
        num_parallel_uploads=3,
        tags={},
        legal_hold=False,
    )


def test_get_object_error(storage, mock_minio):
    mock_minio.get_object.side_effect = Exception("Get object failed")
    with pytest.raises(Exception, match="Get object failed"):
        storage.get_object("error-bucket", "error-object")


def test_fget_object_error(storage, mock_minio):
    mock_minio.fget_object.side_effect = Exception("Download failed")
    with pytest.raises(Exception, match="Download failed"):
        storage.fget_object("error-bucket", "error-object", "/error/path")


def test_fput_object_error(storage, mock_minio):
    mock_minio.fput_object.side_effect = Exception("Upload failed")
    with pytest.raises(Exception, match="Upload failed"):
        storage.fput_object("error-bucket", "error-object", "/error/path")
