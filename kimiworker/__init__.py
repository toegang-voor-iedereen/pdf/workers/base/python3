# __init__.py
from .version import version

__version__ = version

from .worker import Worker
from .kimi_logger import KimiLogger
from .types.job_handler import JobHandler
from .types.worker_job import WorkerJob
from .types.publish_method import PublishMethod
from .types.remote_file_storage import (
    RemoteFileStorageInterface,
    RemoteFileStorageWriteResult,
)
from .types.objects import (
    ContentAttribute,
    BoundingBox,
    ContentLabel,
    ContentDefinition,
)
from .utils.bbox import (
    convert_bounding_box,
    bounding_box_to_dict,
    bounding_box_to_polygon,
    border_polygons,
    get_enclosing_bbox,
)

from .utils.dicts import merge_dicts

from .utils.worker_job import extract_best_attribute_value

from .uri import *
