"""
The job_listener module contains the JobListener class, which is responsible for listening to incoming job messages,
processing them, and reporting worker availability and status.

This class provides functionality for creating connections to the AMQP server, declaring queues and exchanges,
handling incoming job messages, processing jobs, sending heartbeats, reporting availability and status, and more.

The JobListener class interacts with the KimiLogger for logging, the Minio client for file operations,
and the JobHandler protocol for processing jobs.
"""

import datetime
import hashlib
import io
import json
import random
import signal
import sys
import tempfile
import threading
import time
import uuid
from typing import Optional

import pika
import pika.spec
import pika.exceptions
from minio import Minio
from pika.adapters.blocking_connection import BlockingChannel
from pika.delivery_mode import DeliveryMode

from kimiworker.connection_manager import ConnectionManager
from kimiworker.config import AMQPConfig
from kimiworker.types.job_handler import JobHandler
from kimiworker.types.worker_job import WorkerJob
from kimiworker.types.worker_scout import WorkerScout

from .kimi_logger import KimiLogger
from .minio_remote_file_storage import MinioRemoteFileStorage
from .version import version as kimi_baseworker_version


class JobListener:
    """
    JobListener class listens for and processes incoming job messages.
    """

    queue_name: str
    heartbeat_interval: float
    available_status_ttl: int
    instance_trace_id: str
    auto_download: bool
    store_debug_job: bool = True
    number_of_concurrent_jobs: int = 1

    logger: KimiLogger
    minio: Minio
    job_handler: JobHandler

    jobs_exchange: str
    results_exchange: str
    health_exchange: str

    dispatcher_thread: Optional[threading.Thread]
    dispatcher_connection: Optional[pika.BlockingConnection]
    stop_dispatcher_event: threading.Event

    num_partitions: int
    partitioned_queue_names: list[str]
    _heartbeat_timeout_id: Optional[object] = None

    def __init__(
        self,
        logger: KimiLogger,
        minio: Minio,
        job_handler: JobHandler,
        queue_name: str,
        amqp_config: AMQPConfig,
        hostname: str,
        auto_download=False,
    ):
        """
        Initialize the JobListener instance.

        Args:
            logger (KimiLogger): Logger instance.
            minio (Minio): Minio client instance.
            job_handler (JobHandler): Handler function for processing jobs.
            queue_name (str): The name of the queue to listen on.
            amqp_config (AMQPConfig): AMQP configuration settings.
            hostname (str): used as worker_instance_name and consumer_tag
            auto_download (bool, optional): Flag indicating whether to automatically download job files. Defaults to False.
        """

        self.amqp_host = amqp_config.host
        self.amqp_port = amqp_config.port
        self.amqp_username = amqp_config.username
        self.amqp_password = amqp_config.password
        self.num_partitions = amqp_config.num_partitions

        self.worker_instance_name = self.consumer_tag = hostname

        self.exchange_prefix = amqp_config.exchange
        self.health_exchange = f"{self.exchange_prefix}-workers"
        self.jobs_exchange = f"{self.exchange_prefix}-jobs"
        self.results_exchange = f"{self.exchange_prefix}-job-results"

        self.health_routing_key = "station.*"
        self.jobs_routing_key = "job.*"
        self.job_acks_routing_key = "ack."
        self.results_routing_key = "result."

        self.heartbeat_interval = amqp_config.heartbeat_interval
        self.available_status_ttl = int(self.heartbeat_interval * 2000)
        self.instance_trace_id = str(uuid.uuid4())
        self.logger = logger
        self.minio = minio
        self.job_handler = job_handler
        self.queue_name = queue_name
        self.auto_download = auto_download
        self.dispatcher_thread = None
        self.dispatcher_connection = None
        self.stop_dispatcher_event = threading.Event()

        self.connection_manager = ConnectionManager(
            amqp_config=amqp_config,
            client_properties={
                "product": self.worker_instance_name,
                "version": kimi_baseworker_version,
            },
            logger=self.logger,
        )
        self.dispatcher_connection_manager = ConnectionManager(
            amqp_config=amqp_config,
            client_properties={
                "product": f"{self.worker_instance_name}_dispatcher",
                "version": kimi_baseworker_version,
            },
            logger=self.logger,
        )

        self.channel: Optional[BlockingChannel] = None
        self.publishing_channel: Optional[BlockingChannel] = None

        self.__register_interrupt_handler()

        self.partitioned_queue_names = [
            f"{self.queue_name}_part_{i}" for i in range(self.num_partitions)
        ]

    def __register_interrupt_handler(self):
        """
        Register interrupt signal handlers.
        """
        interrupt_logger = self.logger.child({"phase": "interrupt"})

        def signal_handler(signal, frame):
            reason = f"Received interrupt signal: {signal}"
            interrupt_logger.warning(reason)

            self.stop_heartbeat()
            self.stop_dispatcher_event.set()

            if self.dispatcher_thread is not None:
                interrupt_logger.info("Waiting for dispatcher thread to terminate...")
                self.dispatcher_thread.join()
                interrupt_logger.info("Dispatcher thread terminated.")

            with self.connection_manager.get_channel() as channel:
                self.report_interrupted(channel=channel, reason=reason)

            interrupt_logger.info("Closing channels and connections")

            self.connection_manager.close()
            self.dispatcher_connection_manager.close()

            interrupt_logger.info("Exiting")
            sys.exit(0)

        signal.signal(signal.SIGINT, signal_handler)
        signal.signal(signal.SIGTERM, signal_handler)

    def start(self, number_of_concurrent_jobs=1):
        """
        Start processing jobs.

        Args:
            number_of_concurrent_jobs (int, optional): Number of concurrent jobs to process. Defaults to 1.
        """
        self.number_of_concurrent_jobs = number_of_concurrent_jobs
        try:
            self.setup_main_connection(number_of_concurrent_jobs)
        except pika.exceptions.AMQPConnectionError as e:
            self.logger.critical(
                f"Initial connection to RabbitMQ failed: {e}.  Exiting."
            )
            sys.exit(1)

        self.dispatcher_thread = threading.Thread(
            target=self.start_dispatcher_consumption
        )
        self.dispatcher_thread.daemon = True
        self.dispatcher_thread.start()
        self.start_listening()

    def setup_main_connection(self, number_of_concurrent_jobs):
        """
        Sets up the main connection and channel for job processing.
        """
        self.channel = self.connection_manager.get_channel()
        self.publishing_channel = self.connection_manager.get_channel()
        self.set_prefetch(prefetch_count=number_of_concurrent_jobs)
        self.declare_base_queue()
        self.declare_partitioned_queues()
        self.declare_exchanges()
        self.bind_base_queue()

    def start_listening(self):
        with self.connection_manager.get_channel() as channel:
            self.send_heartbeat(channel=channel, reason="Worker started")
        self.logger.info("Start listening for jobs")

        while not self.stop_dispatcher_event.is_set():
            try:
                with self.connection_manager.get_channel() as channel:
                    queue_names = list(self.partitioned_queue_names)

                    if len(queue_names) > 1:
                        random.shuffle(queue_names)

                    message_found = False

                    for queue_name in queue_names:
                        try:
                            method_frame, header_frame, body = channel.basic_get(
                                queue=queue_name, auto_ack=False
                            )
                            if method_frame:
                                message_found = True
                                self.handle_message(
                                    channel, method_frame, header_frame, body
                                )
                                break
                        except pika.exceptions.AMQPError as e:
                            self.logger.error(
                                f"AMQP error getting message from {queue_name}: {e}"
                            )
                            break

                    if not message_found:
                        time.sleep(0.1)

            except Exception as e:
                self.logger.error(f"Unexpected error in main listening loop: {e}")
                time.sleep(0.1)

    def start_dispatcher_consumption(self):
        self.logger.info(f"Dispatcher thread: consuming from '{self.queue_name}'")

        while not self.stop_dispatcher_event.is_set():
            try:
                with self.dispatcher_connection_manager.get_channel() as channel:

                    def consume_callback(ch, method, properties, body):
                        if self.stop_dispatcher_event.is_set():
                            self.logger.info("Dispatcher thread: stop signal received.")
                            ch.stop_consuming()
                            return
                        self.dispatch_message(ch, method, properties, body)

                    channel.basic_consume(
                        queue=self.queue_name,
                        on_message_callback=consume_callback,
                        consumer_tag=f"{self.consumer_tag}_dispatcher",
                    )

                    self.logger.info("Starting to consume messages")

                    while (
                        channel.connection
                        and channel.connection.is_open
                        and not self.stop_dispatcher_event.is_set()
                    ):
                        try:
                            channel.connection.process_data_events(time_limit=1)
                        except pika.exceptions.AMQPError as e:
                            self.logger.error(
                                f"Dispatcher AMQP error: {e}. Will reconnect..."
                            )
                            break
                        except Exception as e:
                            if self.stop_dispatcher_event.is_set():
                                break
                            self.logger.error(
                                f"Unexpected error in dispatcher event loop: {e}"
                            )
                            break

                    try:
                        if channel.is_open:
                            channel.stop_consuming()
                    except Exception as e:
                        self.logger.warning(f"Error stopping consumer: {e}")

            except Exception as e:
                self.logger.error(f"Error establishing connection: {e}")

            if not self.stop_dispatcher_event.is_set():
                self.logger.info("Waiting before reconnection attempt...")
                time.sleep(5)

        self.logger.info("Dispatcher thread: exiting")

    def set_prefetch(self, prefetch_count: int):
        """
        Sets prefetch.
        """
        if self.channel is None:
            raise ValueError("Main channel is not initialized.")
        self.logger.info(f"Setting prefetch to {prefetch_count} per partition")
        self.channel.basic_qos(prefetch_count=prefetch_count)

    def declare_base_queue(self):
        """
        Declares the base queue.
        """
        if self.channel is None:
            raise ValueError("Main channel is not initialized.")
        self.logger.info(f"Declaring base queue {self.queue_name}")
        self.channel.queue_declare(
            queue=self.queue_name, durable=True, arguments={"x-queue-type": "quorum"}
        )

    def declare_partitioned_queues(self):
        """
        Declares partitioned queues.
        """
        if self.channel is None:
            raise ValueError("Main channel is not initialized.")
        for queue_name in self.partitioned_queue_names:
            self.logger.info(f"Declaring partitioned queue {queue_name}")
            self.channel.queue_declare(
                queue=queue_name, durable=True, arguments={"x-queue-type": "quorum"}
            )

    def declare_exchanges(self):
        """
        Declares exchanges.
        """
        self.declare_exchange(self.health_exchange)
        self.declare_exchange(self.jobs_exchange)
        self.declare_exchange(self.results_exchange)

    def declare_exchange(self, exchange_name: str):
        """
        Declare exchange on AMQP server.

        Args:
            exchange_name (str): Name of the exchange to declare.
        """
        if self.channel is None:
            raise ValueError("Main channel is not initialized.")
        self.logger.info(f"Declaring exchange {exchange_name}")
        self.channel.exchange_declare(
            exchange=exchange_name, exchange_type="fanout", durable=True
        )

    def bind_base_queue(self):
        """
        Binds the base queue.
        """
        with self.connection_manager.get_channel() as temp_channel:
            self.bind_exchange(
                self.jobs_exchange,
                self.jobs_routing_key,
                self.queue_name,
                temp_channel,
            )

            self.bind_exchange(
                self.health_exchange,
                self.health_routing_key,
                self.queue_name,
                temp_channel,
            )

    def bind_exchange(
        self,
        exchange_name: str,
        routing_key: str,
        queue_name: str,
        channel: BlockingChannel,
    ):
        """
        Binds a queue to an exchange.

        Args:
            exchange_name (str): Name of the exchange to bind to
            routing_key (str): Routing key
            queue_name (str): Name of the queue to bind to the exchange
            channel: (BlockingChannel): Channel used to perform the queue to exchange binding
        """
        self.logger.info(
            f"Binding exchange {exchange_name} to queue {queue_name} with routing key '{routing_key}'"
        )
        channel.queue_bind(queue_name, exchange_name, routing_key)

    def report_interrupted(self, channel: BlockingChannel, reason: str):
        """
        Report worker interruption.

        Args:
            channel (BlockingChannel): AMQP Channel.
            reason (str): Reason for interruption.
        """
        self.report_availability(channel, "interrupted", reason)

    def stop_heartbeat(self):
        """
        Stop sending heartbeats.  Removes the scheduled task from Pika's I/O loop.
        """
        if self._heartbeat_timeout_id and self.connection_manager.connection:
            try:
                self.connection_manager.connection.add_callback_threadsafe(
                    self._remove_heartbeat_timeout()
                )
            except Exception as e:
                self.logger.warning(f"Error removing heartbeat timeout: {e}")

    def _remove_heartbeat_timeout(self):
        """Removes the heartbeat timeout (called via add_callback_threadsafe)."""
        if self.connection_manager.connection:
            try:
                self.connection_manager.connection.remove_timeout(
                    self._heartbeat_timeout_id
                )
            except Exception as e:
                self.logger.warning(f"Error in _remove_heartbeat_timeout: {e}")
        self._heartbeat_timeout_id = None

    def send_heartbeat(self, channel: BlockingChannel, reason="Heartbeat"):
        """
        Send heartbeat message using Pika's I/O loop.

        Args:
            channel (BlockingChannel): AMQP Channel.
            reason (str): Reason for heartbeat (defaults to Heartbeat).
        """
        self.report_availability(channel, "active", reason)

        if (
            self.connection_manager.connection
            and self.connection_manager.connection.is_open
        ):
            self.connection_manager.connection.add_callback_threadsafe(
                self._schedule_next_heartbeat
            )
        else:
            self.logger.warning("Cannot send heartbeat: connection is not open.")

    def _schedule_next_heartbeat(self):
        """Schedules the next heartbeat, handling potential connection issues."""
        if (
            self.connection_manager.connection
            and self.connection_manager.connection.is_open
        ):
            self._heartbeat_timeout_id = self.connection_manager.connection.call_later(
                self.heartbeat_interval, self.send_heartbeat
            )
        else:
            self.logger.warning("Heartbeat not scheduled: connection is closed.")

    def report_availability(
        self, channel: BlockingChannel, availability: str, reason: str
    ):
        """
        Report worker availability or unavailability.

        Args:
            channel (BlockingChannel): AMQP Channel.
            availability (str): Availability status ("active", "interrupted", "stopped").
            reason (str): Reason for the availability status change.
        """
        if availability == "active":
            self.logger.trace(f"Reporting availability ({reason})")
        else:
            self.logger.info(f"Reporting availability '{availability}' ({reason})")

        report = {
            "workerType": self.exchange_prefix,
            "workerName": self.queue_name,
            "workerInstance": self.worker_instance_name,
            "state": availability,
            "reason": reason,
            "timestamp": f"{datetime.datetime.now().isoformat()}",
        }

        expiration: Optional[str] = None
        if availability == "active":
            expiration = f"{self.available_status_ttl}"

        headers = self.create_message_headers(self.instance_trace_id)

        properties = pika.BasicProperties(
            content_type="application/json",
            content_encoding="utf8",
            priority=10,
            expiration=expiration,
            delivery_mode=DeliveryMode.Persistent,
            headers=headers,
        )

        channel.basic_publish(
            self.results_exchange,
            f"worker.{self.exchange_prefix}.{self.queue_name}",
            json.dumps(report),
            properties,
        )

    def dispatch_message(
        self,
        channel: BlockingChannel,
        method: pika.spec.Basic.Deliver,
        properties: pika.spec.BasicProperties,
        body: bytes,
    ):
        """
        Dispatches messages (runs in dispatcher thread).

        Args:
            channel (BlockingChannel): Channel for communication with AMQP server.
            method (pika.spec.Basic.Deliver): Method object containing delivery information.
            properties (pika.spec.BasicProperties): Properties of the message.
            body (bytes): Message body.
        """
        try:
            trace_id = properties.headers.get("x-trace-id", str(uuid.uuid4()))

            routing_key = method.routing_key

            logger_data = {
                "trace": {"id": trace_id},
                "routingKey": routing_key,
            }

            logger = self.logger.child(logger_data)

            if routing_key == "station.workers":
                self.handle_station_scout(body=body, logger=logger, channel=channel)
            else:
                queue_name = self.get_partitioned_queue_name(trace_id)

                channel.basic_publish(
                    exchange="",
                    routing_key=queue_name,
                    body=body,
                    properties=properties,
                )
                logger.info(f"Dispatched message to {queue_name}")

            channel.basic_ack(delivery_tag=method.delivery_tag)

        except pika.exceptions.AMQPError as e:
            self.logger.error(f"AMQP Error dispatching message: {e}. Nacking.")
            channel.basic_nack(delivery_tag=method.delivery_tag, requeue=False)
        except Exception as e:
            self.logger.error(f"Error dispatching message: {e}. Nacking.")
            channel.basic_nack(delivery_tag=method.delivery_tag, requeue=False)

    def get_partitioned_queue_name(self, hashable: str) -> str:
        """
        Calculates the hashed queue name using multiply-shift.
        Multiplies the lower 32 bits of a sha256 hash by the number of
        partitions and right-shifts by 32 to get a consistent index.

        Args:
            uid (str): string used to determine the partition name
        """
        hashable_encoded = str(hashable).encode("utf-8")
        hash_value = int(hashlib.sha256(hashable_encoded).hexdigest(), 16)
        hash_value_32bit = hash_value & 0xFFFFFFFF

        queue_index = (hash_value_32bit * self.num_partitions) >> 32

        return f"{self.queue_name}_part_{queue_index}"

    def handle_message(
        self,
        channel: BlockingChannel,
        method: pika.spec.Basic.Deliver,
        properties: pika.spec.BasicProperties,
        body: bytes,
    ):
        """
        Handles messages from partitioned queues

        Args:
            channel (BlockingChannel): Channel for communication with AMQP server.
            method (pika.spec.Basic.Deliver): Method object containing delivery information.
            properties (pika.spec.BasicProperties): Properties of the message.
            body (bytes): Message body.
        """
        headers = getattr(properties, "headers", {})
        trace_id: str = headers.get("x-trace-id", str(uuid.uuid4()))
        routing_key = method.routing_key

        logger_data = {
            "trace": {"id": trace_id},
            "routingKey": routing_key,
        }

        # Get all headers starting with x-kimi- and use them as camel-cased variables in the logger child data
        for header_name, header_value in {
            key: value
            for key, value in headers.items()
            if isinstance(key, str) and key.startswith("x-kimi")
        }.items():
            try:
                variable_name_kebab_case = header_name.removeprefix("x-kimi-").lower()
                variable_name = (
                    variable_name_kebab_case[0]
                    + "".join(
                        x.capitalize() for x in variable_name_kebab_case.split("-")
                    )[1:]
                )
                logger_data[variable_name] = header_value
            except (AttributeError, IndexError):
                continue

        logger = self.logger.child(logger_data)

        delivery_tag = method.delivery_tag
        content_type = properties.content_type

        logger.debug(
            f"Received new message, {len(body)} bytes. DeliveryTag: {delivery_tag}."
        )

        if content_type is None:
            logger.child({"messageProperties": properties}).error(
                "Missing content_type property, Nacking and skipping message"
            )
            channel.basic_nack(method.delivery_tag, multiple=False, requeue=False)
            return

        if content_type != "application/json":
            logger.error("Unknown message content type, ignoring")
            channel.basic_nack(method.delivery_tag, multiple=False, requeue=False)
            return

        body_string = body.decode()

        if body_string is None:
            logger.error("Failed to decode body")
            return

        if routing_key.startswith(self.queue_name):
            self.handle_job_message(
                channel=channel,
                method=method,
                properties=properties,
                body_string=body_string,
                logger=logger,
                trace_id=trace_id,
            )
            return

        logger.warning(
            f"Received message with unexpected routing key: '{routing_key}'. Nacking..."
        )
        channel.basic_nack(method.delivery_tag, multiple=False, requeue=False)

    def handle_station_scout(
        self,
        channel: BlockingChannel,
        body: bytes,
        logger: KimiLogger,
    ):
        """
        Handle station scout message.

        Args:
            channel (BlockingChannel): Channel for communication with AMQP server.
            body (bytes): Message body.
            logger (KimiLogger): Logger instance.
        """
        logger.info("Parsing JSON")
        body_string = body.decode()
        scout: WorkerScout = json.loads(body_string)
        self.report_availability(
            channel, "active", reason=f"Answering Worker Scout: {scout.get('reason')}"
        )

    def handle_job_message(
        self,
        channel: BlockingChannel,
        method: pika.spec.Basic.Deliver,
        properties: pika.spec.BasicProperties,
        body_string: str,
        trace_id: str,
        logger: KimiLogger,
    ):
        """
        Handles a job message.

        Args:
            channel (BlockingChannel): Channel for communication with AMQP server.
            method (pika.spec.Basic.Deliver): Method object containing delivery information.
            properties (pika.spec.BasicProperties): Properties of the message.
            body_string (str): Message body.
            trace_id (str): Trace id.
            logger (KimiLogger): Logger instance.
        """
        job_id = str(uuid.uuid4())
        logger = logger.child({"jobId": job_id})

        try:
            job: WorkerJob = json.loads(body_string)
            if job is None:
                logger.error("Failed to parse WorkerJob. Nacking...")
                channel.basic_nack(method.delivery_tag, multiple=False, requeue=False)
                return

            self.publish_job_ack(
                logger=logger,
                trace_id=trace_id,
                record_id=job.get("recordId"),
                job_id=job_id,
            )

            self.handle_job(logger, trace_id, method.delivery_tag, job_id, job)

            channel.basic_ack(method.delivery_tag)

        except Exception as e:
            logger.error(f"Error handling message: {e}")
            channel.basic_nack(method.delivery_tag, multiple=False, requeue=True)

    def store_job_in_minio(
        self, job: WorkerJob, job_id: str, logger: KimiLogger, trace_id: str
    ):
        """
        Store the job details in MinIO for debugging purposes.

        Args:
            job (WorkerJob): Job data.
            job_id (str): ID of the job.
            logger (KimiLogger): Logger instance.
            trace_id (str): Trace id.
        """
        if not self.store_debug_job:
            return

        bucket_name = "kimi-debug"
        folder_name = job.get("recordId")
        file_name = f"job.{self.queue_name}.{job_id}.json"
        logger.child(
            {
                "bucket_name": bucket_name,
                "folder_name": folder_name,
                "file_name": file_name,
            }
        ).debug("Storing job debug file in MinIO...")

        json_content = json.dumps(job)
        json_data = io.BytesIO(json_content.encode("utf8"))
        self.minio.put_object(
            bucket_name=bucket_name,
            object_name=f"{folder_name}/{file_name}",
            data=json_data,
            length=len(json_content),
            content_type="application/json",
            metadata={
                "jobId": job_id,
                "recordId": job.get("recordId"),
                "worker": self.queue_name,
                "instance": self.instance_trace_id,
                "traceId": trace_id,
            },
        )
        return True

    def handle_job(
        self,
        logger: KimiLogger,
        trace_id: str,
        delivery_tag: int,
        job_id: str,
        job: WorkerJob,
    ):
        """
        Handle job.

        Args:
            logger (KimiLogger): Logger instance.
            trace_id (str): Trace ID of the job.
            delivery_tag (int): Delivery tag of the message.
            job_id (str): ID of the job.
            job (WorkerJob): Job data.
        """
        logger = logger.child({"phase": "handleJob", "deliveryTag": delivery_tag})
        logger.info("Handling job")

        bucket_name = job.get("bucketName")
        filename = job.get("filename")

        logger.event("workerJobStarted")

        logger = logger.child({"bucketName": bucket_name, "bucketFilename": filename})

        logger.info("Creating workspace for job")

        self.store_job_in_minio(job, job_id, logger, trace_id)

        with tempfile.TemporaryDirectory() as temp_path:
            local_file_path = ""

            if self.auto_download:
                local_file_path = f"{temp_path}/{job_id}.jpg"
                download_succeeded = self.download_job_file(
                    logger, bucket_name, filename, local_file_path
                )

                if not download_succeeded:
                    raise ValueError("Could not download job file")

            logger.info("Sending job to jobHandler")

            def publish_result_method(
                result_key: str, result, success: bool = True, confidence: float = 100
            ):
                self.publish_result(
                    logger,
                    trace_id,
                    job,
                    job_id,
                    result_key,
                    result,
                    success,
                    confidence,
                )

            job_start_time = time.perf_counter()
            job_handler_completed = False

            try:
                handler_logger = logger.child({"phase": "Handler"})
                handler_storage = MinioRemoteFileStorage(minio=self.minio)

                if self.auto_download:
                    self.job_handler(
                        log=handler_logger,
                        job=job,
                        job_id=job_id,
                        local_file_path=local_file_path,
                        remote_file_storage=handler_storage,
                        publish=publish_result_method,
                    )
                else:
                    self.job_handler(
                        handler_logger,
                        job,
                        job_id,
                        f"{bucket_name}/{filename}",
                        remote_file_storage=handler_storage,
                        publish=publish_result_method,
                    )
                job_handler_completed = True
            except Exception as e:
                logger.child({"exception": repr(e)}).error(
                    "Something went wrong when handling the job."
                )

            process_time_in_seconds = time.perf_counter() - job_start_time
            logger.info(f"Finished job in {process_time_in_seconds} seconds")
            logger.event(
                "workerJobCompleted", {"durationSeconds": process_time_in_seconds}
            )

            if not job_handler_completed:
                publish_result_method(
                    "stringResult",
                    "Unknown error in job handling",
                    success=False,
                    confidence=0,
                )

    def download_job_file(
        self, logger: KimiLogger, bucket_name: str, filename: str, target_path: str
    ):
        """
        Download job file from Minio.

        Args:
            logger (KimiLogger): Logger instance.
            bucket_name (str): Name of the bucket containing the file.
            filename (str): Name of the file to download.
            target_path (str): Local path to save the downloaded file.

        Returns:
            bool: True if the download is successful, False otherwise.
        """
        logger.info(f"Downloading job file from to bucket {bucket_name}")
        bucket_found = self.minio.bucket_exists(bucket_name)

        if not bucket_found:
            logger.error("Cannot find bucket from job")
            return False

        logger.info(f"Fetching file: {filename}")

        file_start_time = time.perf_counter()
        try:
            file_object = self.minio.fget_object(bucket_name, filename, target_path)
        except Exception as e:
            logger.error(f"Failed to download file: {e}")
            return False
        download_time_in_seconds = time.perf_counter() - file_start_time
        logger.info(f"File downloaded in {download_time_in_seconds} seconds")
        logger.event(
            "workerJobFileDownloaded", {"durationSeconds": download_time_in_seconds}
        )

        if file_object is None:
            logger.error("Failed to download file (fget_object error)")
            return False

        logger.child({"localFilePath": target_path}).info(
            f"File downloaded to {target_path}"
        )
        return True

    def publish_job_ack(
        self, logger: KimiLogger, trace_id: str, record_id: str, job_id: str
    ):
        """
        Publish job acknowledgment event.

        Args:
            logger (KimiLogger): Logger instance.
            trace_id (str): Trace ID of the job.
            record_id (str): ID of the record.
            job_id (str): ID of the job.
        """
        logger.info(
            f"Publishing job acknowledgement for record '{record_id}' with traceId '{trace_id}'"
        )
        ack_json = json.dumps({"recordId": record_id, "jobId": job_id})
        properties = pika.spec.BasicProperties(
            priority=5, headers=self.create_message_headers(trace_id)
        )

        with self.connection_manager.get_channel() as publish_channel:
            publish_channel.basic_publish(
                exchange=self.results_exchange,
                routing_key=f"{self.job_acks_routing_key}{job_id}",
                body=ack_json,
                properties=properties,
            )
        logger.event("workerJobAckSent")

    def publish_result(
        self,
        logger: KimiLogger,
        trace_id: str,
        job: WorkerJob,
        job_id: str,
        result_key: str,
        result,
        success: bool,
        confidence: float,
    ):
        """
        Publish job result message.

        Args:
            logger (KimiLogger): Logger instance.
            trace_id (str): Trace ID of the job.
            job (WorkerJob): Job data.
            job_id (str): ID of the job.
            result_key (str): Key of the result.
            result: Result of the job.
            success (bool): Flag indicating whether the job was successful.
            confidence (int): Confidence level of the result.
        """
        logger.info("Publishing result")

        job_result_report = {
            "traceId": trace_id,
            "recordId": job.get("recordId"),
            "jobId": job_id,
            "timestamp": f"{datetime.datetime.now().isoformat()}",
            "success": success,
            "confidence": confidence,
        }

        if success:
            job_result_report[result_key] = result
        else:
            job_result_report["error"] = {"code": 0, "message": result}

        job_result_report_json = json.dumps(job_result_report)
        headers = self.create_message_headers(trace_id=trace_id)
        properties = pika.spec.BasicProperties(headers=headers)

        with self.connection_manager.get_channel() as publish_channel:
            publish_channel.basic_publish(
                exchange=self.results_exchange,
                routing_key=f"{self.results_routing_key}{job_id}",
                body=job_result_report_json,
                properties=properties,
            )
        logger.event("workerJobResultSent")

    def create_message_headers(self, trace_id):
        """
        Create message headers.

        Args:
            trace_id (str): Trace ID of the message.

        Returns:
            dict: Dictionary containing message headers.
        """
        return {
            "x-kimi-worker-name": self.queue_name,
            "x-kimi-worker-instance-name": self.worker_instance_name,
            "x-trace-id": trace_id,
            "timestamp": f"{datetime.datetime.now().isoformat()}",
        }
