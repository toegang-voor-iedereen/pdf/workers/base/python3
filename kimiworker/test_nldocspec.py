from typing import Generic, List, TypeVar

from pydantic import BaseModel

from .types.nldocspec import (
    BlockQuotation,
    Document,
    Image,
    Heading,
    Paragraph,
    Table,
    TableCell,
    TableHeader,
    TableRow,
    UnorderedList,
    OrderedList,
    ListItem,
    Definition,
    DefinitionList,
    Text,
    Link,
    InlineQuotation,
    ObjectDescriptor,
    BooleanDescriptor,
    StringDescriptor,
    NumberDescriptor,
    IntegerDescriptor,
)

ScenarioType = TypeVar(
    "ScenarioType",
)


class ValidationScenario(BaseModel, Generic[ScenarioType]):
    scenario: str
    examples: List[ScenarioType]


class ValidationFile(BaseModel):
    BlockQuotation: List[ValidationScenario[BlockQuotation]]
    Document: List[ValidationScenario[Document]]
    Image: List[ValidationScenario[Image]]
    Heading: List[ValidationScenario[Heading]]
    Paragraph: List[ValidationScenario[Paragraph]]
    Table: List[ValidationScenario[Table]]
    TableCell: List[ValidationScenario[TableCell]]
    TableHeader: List[ValidationScenario[TableHeader]]
    TableRow: List[ValidationScenario[TableRow]]
    UnorderedList: List[ValidationScenario[UnorderedList]]
    OrderedList: List[ValidationScenario[OrderedList]]
    ListItem: List[ValidationScenario[ListItem]]
    Definition: List[ValidationScenario[Definition]]
    DefinitionList: List[ValidationScenario[DefinitionList]]
    Text: List[ValidationScenario[Text]]
    Link: List[ValidationScenario[Link]]
    InlineQuotation: List[ValidationScenario[InlineQuotation]]
    ObjectDescriptor: List[ValidationScenario[ObjectDescriptor]]
    BooleanDescriptor: List[ValidationScenario[BooleanDescriptor]]
    StringDescriptor: List[ValidationScenario[StringDescriptor]]
    NumberDescriptor: List[ValidationScenario[NumberDescriptor]]
    IntegerDescriptor: List[ValidationScenario[IntegerDescriptor]]


AnySpecType = (
    BlockQuotation
    | Document
    | Image
    | Heading
    | Paragraph
    | Table
    | TableCell
    | TableHeader
    | TableRow
    | UnorderedList
    | OrderedList
    | ListItem
    | Definition
    | DefinitionList
    | Text
    | Link
    | InlineQuotation
    | ObjectDescriptor
    | BooleanDescriptor
    | StringDescriptor
    | NumberDescriptor
    | IntegerDescriptor
)

ValidationFile.model_rebuild()


def test_nldocspec_json_validation():
    with open("kimiworker/test_utils/nldocspec.valid.json") as all_resources_json:
        ValidationFile.model_validate_json(all_resources_json.read(), strict=True)
