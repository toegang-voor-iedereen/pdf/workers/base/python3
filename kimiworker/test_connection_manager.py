import pytest
from unittest.mock import Mock, patch
import pika
import pika.exceptions
from pika.adapters.blocking_connection import BlockingChannel
from kimiworker.config import AMQPConfig
from kimiworker.connection_manager import ConnectionManager


@pytest.fixture
def amqp_config():
    return AMQPConfig(
        host="localhost",
        port="5672",
        username="guest",
        password="guest",
        heartbeat_interval=600,
    )


@pytest.fixture
def client_properties():
    return {"client_name": "test_client"}


@pytest.fixture
def logger():
    mock_logger = Mock()
    mock_logger.child.return_value = mock_logger
    return mock_logger


def test_connect_with_retry_success(amqp_config, client_properties, logger):
    """Test connection with retry that succeeds on second attempt."""
    mock_sleep = Mock()
    mock_connection = Mock()
    mock_connection.side_effect = [pika.exceptions.AMQPError("fail1"), Mock()]

    manager = ConnectionManager(
        amqp_config,
        client_properties,
        retry_attempts=2,
        retry_delay=lambda x: 1,
        sleep_fn=mock_sleep,
        connection_factory=lambda props, config: mock_connection(props, config),
        logger=logger,
    )

    assert manager.connection is not None
    assert mock_connection.call_count == 2
    mock_sleep.assert_called_once_with(1)
    logger.error.assert_called_once_with("AMQP: connection error: fail1")
    logger.info.assert_any_call("AMQP: Retrying in 1 seconds...")


def test_connect_failure_after_retries(amqp_config, client_properties, logger):
    """Test connection failing after all retries."""
    mock_sleep = Mock()
    mock_connection = Mock()
    mock_connection.side_effect = [pika.exceptions.AMQPError("fail")] * 2

    with pytest.raises(pika.exceptions.AMQPError):
        ConnectionManager(
            amqp_config,
            client_properties,
            retry_attempts=2,
            retry_delay=lambda x: 1,
            sleep_fn=mock_sleep,
            connection_factory=lambda props, config: mock_connection(props, config),
            logger=logger,
        )

    assert mock_connection.call_count == 2
    mock_sleep.assert_called_once_with(1)
    logger.error.assert_called()


def test_get_channel_success(amqp_config, client_properties, logger):
    """Test getting a channel successfully."""
    mock_connection_instance = Mock()
    mock_channel = Mock(spec=BlockingChannel)
    mock_connection_instance.channel.return_value = mock_channel

    manager = ConnectionManager(
        amqp_config,
        client_properties,
        retry_attempts=1,
        connection_factory=lambda props, config: mock_connection_instance,
        logger=logger,
    )

    channel = manager.get_channel()

    assert channel == mock_channel
    mock_connection_instance.channel.assert_called_once()


def test_get_channel_reconnect(amqp_config, client_properties, logger):
    """Test getting a channel with reconnection after failure."""
    mock_connection_instance = Mock()
    mock_channel = Mock(spec=BlockingChannel)
    mock_connection_instance.channel.side_effect = [
        pika.exceptions.AMQPError("channel fail"),
        mock_channel,
    ]
    mock_connection = Mock(return_value=mock_connection_instance)

    manager = ConnectionManager(
        amqp_config,
        client_properties,
        retry_attempts=1,
        connection_factory=lambda props, config: mock_connection(props, config),
        logger=logger,
    )

    channel = manager.get_channel()

    assert channel == mock_channel
    assert mock_connection.call_count == 3  # Initial + 2 reconnect attempts
    logger.error.assert_called_once()


def test_close_success(amqp_config, client_properties, logger):
    """Test closing the connection successfully."""
    mock_connection_instance = Mock()
    mock_connection_instance.is_open = True

    manager = ConnectionManager(
        amqp_config,
        client_properties,
        retry_attempts=1,
        connection_factory=lambda props, config: mock_connection_instance,
        logger=logger,
    )
    manager.close()

    mock_connection_instance.close.assert_called_once()
    assert manager.connection is None
    logger.info.assert_any_call("AMQP: connection closed.")


def test_is_connected_true(amqp_config, client_properties, logger):
    """Test is_connected returns True when connection is open."""
    mock_connection_instance = Mock()
    mock_connection_instance.is_open = True

    manager = ConnectionManager(
        amqp_config,
        client_properties,
        retry_attempts=1,
        connection_factory=lambda props, config: mock_connection_instance,
        logger=logger,
    )

    assert manager.is_connected() is True


def test_is_connected_false_when_closed(amqp_config, client_properties, logger):
    """Test is_connected returns False when connection is closed."""
    mock_connection_instance = Mock()
    mock_connection_instance.is_open = False

    manager = ConnectionManager(
        amqp_config,
        client_properties,
        retry_attempts=1,
        connection_factory=lambda props, config: mock_connection_instance,
        logger=logger,
    )

    assert manager.is_connected() is False


def test_is_connected_false_when_none(amqp_config, client_properties, logger):
    """Test is_connected returns False when connection is None."""
    mock_connection_instance = Mock()

    manager = ConnectionManager(
        amqp_config,
        client_properties,
        retry_attempts=1,
        connection_factory=lambda props, config: mock_connection_instance,
        logger=logger,
    )

    manager.connection = None

    assert manager.is_connected() is False


def test_get_channel_amqp_connection_error(amqp_config, client_properties, logger):
    """Test get_channel handling when connection is None after retries."""
    mock_connection_factory = Mock()

    with patch("kimiworker.connection_manager.ConnectionManager._connect"):
        manager = ConnectionManager(
            amqp_config,
            client_properties,
            retry_attempts=1,
            connection_factory=mock_connection_factory,
            logger=logger,
        )

        manager.connection = None

        with pytest.raises(pika.exceptions.AMQPConnectionError) as exc_info:
            manager.get_channel()

        assert "AMQP: Failed to establish a connection after multiple retries" in str(
            exc_info.value
        )


def test_close_with_amqp_error(amqp_config, client_properties, logger):
    """Test handling of AMQP error during connection close."""
    mock_connection_instance = Mock()
    mock_connection_instance.is_open = True
    mock_connection_instance.close.side_effect = pika.exceptions.AMQPError(
        "close error"
    )

    manager = ConnectionManager(
        amqp_config,
        client_properties,
        retry_attempts=1,
        connection_factory=lambda props, config: mock_connection_instance,
        logger=logger,
    )

    manager.close()

    mock_connection_instance.close.assert_called_once()
    assert manager.connection is None
    logger.warning.assert_called_once_with(
        "AMQP: Error while closing connection: close error"
    )
    logger.info.assert_any_call("AMQP: connection closed.")
