import os
import pytest
from unittest.mock import patch

from kimiworker.config import Config, MinioConfig, AMQPConfig


# These are all the required environment variables with test values
REQUIRED_ENV = {
    # MinIO required vars
    "MINIO_HOST": "localhost",
    "MINIO_PORT": "9000",
    "MINIO_ACCESS_KEY": "test-key",
    "MINIO_SECRET_KEY": "test-secret",
    # AMQP required vars
    "AMQP_HOST": "localhost",
    "AMQP_PORT": "5672",
    "AMQP_USER": "test-user",
    "AMQP_PASS": "test-pass",
    "EXCHANGE": "test-exchange",
    # General required vars
    "HOSTNAME": "test-host",
}


class TestMinioConfig:
    def test_defaults(self):
        """Test MinIO config with minimal required values"""
        with patch.dict(os.environ, REQUIRED_ENV, clear=True):
            config = MinioConfig()
            assert config.host == "localhost"
            assert config.port == "9000"
            assert config.use_ssl is True
            assert config.access_key == "test-key"
            assert config.secret_key == "test-secret"

    def test_custom_values(self):
        """Test MinIO config with custom values"""
        env = REQUIRED_ENV.copy()
        env.update(
            {
                "MINIO_HOST": "minio.example.com",
                "MINIO_PORT": "9001",
                "MINIO_USE_SSL": "false",
            }
        )
        with patch.dict(os.environ, env, clear=True):
            config = MinioConfig()
            assert config.host == "minio.example.com"
            assert config.port == "9001"
            assert config.use_ssl is False

    def test_missing_required_fields(self):
        """Test that MinioConfig exits when required fields are missing"""
        test_cases = [
            {"MINIO_HOST": ""},  # Empty host
            {"MINIO_PORT": ""},  # Empty port
            {"MINIO_ACCESS_KEY": ""},  # Empty access key
            {"MINIO_SECRET_KEY": ""},  # Empty secret key
        ]

        for invalid_var in test_cases:
            env = REQUIRED_ENV.copy()
            env.update(invalid_var)
            with patch.dict(os.environ, env, clear=True):
                with pytest.raises(SystemExit):
                    MinioConfig()


class TestAMQPConfig:
    def test_defaults(self):
        """Test AMQP config with minimal required values"""
        with patch.dict(os.environ, REQUIRED_ENV, clear=True):
            config = AMQPConfig()
            assert config.host == "localhost"
            assert config.port == "5672"
            assert config.heartbeat_interval == 60
            assert config.num_partitions == 20
            assert config.username == "test-user"
            assert config.password == "test-pass"
            assert config.exchange == "test-exchange"

    def test_custom_values(self):
        """Test AMQP config with custom values"""
        env = REQUIRED_ENV.copy()
        env.update(
            {
                "AMQP_HOST": "rabbitmq.example.com",
                "AMQP_PORT": "5673",
                "AMQP_HEARTBEAT_INTERVAL": "30",
                "AMQP_NUM_PARTITIONS": "5",
            }
        )
        with patch.dict(os.environ, env, clear=True):
            config = AMQPConfig()
            assert config.host == "rabbitmq.example.com"
            assert config.port == "5673"
            assert config.heartbeat_interval == 30
            assert config.num_partitions == 5

    def test_missing_required_fields(self):
        """Test that AMQPConfig exits when required fields are missing"""
        test_cases = [
            {"AMQP_HOST": ""},  # Empty host
            {"AMQP_PORT": ""},  # Empty port
            {"AMQP_USER": ""},  # Empty username
            {"AMQP_PASS": ""},  # Empty password
            {"EXCHANGE": ""},  # Empty exchange
        ]

        for invalid_var in test_cases:
            env = REQUIRED_ENV.copy()
            env.update(invalid_var)
            with patch.dict(os.environ, env, clear=True):
                with pytest.raises(SystemExit):
                    AMQPConfig()

    def test_invalid_heartbeat_interval(self):
        """Test that invalid heartbeat interval falls back to default"""
        env = REQUIRED_ENV.copy()
        env["AMQP_HEARTBEAT_INTERVAL"] = "invalid"
        with patch.dict(os.environ, env, clear=True):
            config = AMQPConfig()
            assert config.heartbeat_interval == 60

    def test_invalid_num_partitions(self):
        """Test that invalid num partitions falls back to default"""
        env = REQUIRED_ENV.copy()
        env["AMQP_NUM_PARTITIONS"] = "invalid"
        with patch.dict(os.environ, env, clear=True):
            config = AMQPConfig()
            assert config.num_partitions == 20


class TestConfig:
    def test_initialization(self):
        """Test Config initialization with all required fields"""
        with patch.dict(os.environ, REQUIRED_ENV, clear=True):
            config = Config()
            assert isinstance(config.minio, MinioConfig)
            assert isinstance(config.amqp, AMQPConfig)
            assert config.hostname == "test-host"

    def test_custom_values(self):
        """Test complete configuration with custom values"""
        env = REQUIRED_ENV.copy()
        env.update(
            {
                "MINIO_HOST": "minio.example.com",
                "AMQP_HOST": "rabbitmq.example.com",
                "HOSTNAME": "custom-host",
            }
        )
        with patch.dict(os.environ, env, clear=True):
            config = Config()
            assert config.minio.host == "minio.example.com"
            assert config.amqp.host == "rabbitmq.example.com"
            assert config.hostname == "custom-host"

    def test_missing_hostname(self):
        """Test that Config exits when hostname is missing"""
        env = REQUIRED_ENV.copy()
        env["HOSTNAME"] = ""
        with patch.dict(os.environ, env, clear=True):
            with pytest.raises(SystemExit):
                Config()
