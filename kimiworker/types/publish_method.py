from typing import Protocol, overload, Literal


class PublishMethod(Protocol):
    @overload
    def __call__(
        self,
        result_key: Literal["stringResult"],
        result: str,
        success: bool = True,
        confidence: float = 100,
    ) -> None: ...

    @overload
    def __call__(
        self,
        result_key: Literal["intResult"],
        result: int,
        success: bool = True,
        confidence: float = 100,
    ) -> None: ...

    @overload
    def __call__(
        self,
        result_key: Literal["boolResult"],
        result: bool,
        success: bool = True,
        confidence: float = 100,
    ) -> None: ...

    @overload
    def __call__(
        self,
        result_key: Literal["contentResult"],
        result: list,
        success: bool = True,
        confidence: float = 100,
    ) -> None: ...

    @overload
    def __call__(
        self,
        result_key: Literal["nldocspecResult"],
        result: list,
        success: bool = True,
        confidence: float = 100,
    ) -> None: ...

    @overload
    def __call__(
        self,
        result_key: Literal["error"],
        result: str,
        success: bool = False,
        confidence: float = 0,
    ) -> None: ...

    @overload
    def __call__(
        self,
        result_key: str,
        result: dict,
        success: bool = True,
        confidence: float = 100,
    ) -> None: ...
