"""Objects that describe part of a document"""

from typing import List, Optional, TypedDict

ContentAttribute = int | str | float


class BoundingBox(TypedDict):
    """A bounding box containing percentage wise (0-1) coordinates"""

    top: float
    left: float
    right: float
    bottom: float


class ContentLabel(TypedDict):
    """Content label with confidence value (0 - 100)"""

    name: str
    confidence: float


class ContentDefinition(TypedDict):
    """A content definition object"""

    classification: str
    confidence: float
    bbox: BoundingBox
    attributes: dict[str, ContentAttribute]
    labels: Optional[list[ContentLabel]]
    children: Optional[List["ContentDefinition"]]
