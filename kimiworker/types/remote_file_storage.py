from typing import Dict, List, Tuple, TypedDict, BinaryIO
from builtins import bytes

DictType = Dict[str, str | List[str] | Tuple[str]]


class RemoteFileStorageWriteResult(TypedDict):
    bucket_name: str
    object_name: str
    version_id: str | None
    etag: str | None


class RemoteFileStorageInterface:
    def get_object(
        self,
        bucket_name: str,
        object_name: str,
        offset: int = 0,
        length: int = 0,
        request_headers: DictType | None = None,
        version_id: str | None = None,
        extra_query_params: DictType | None = None,
    ) -> bytes:
        """
        Get data of an object. Returned response should be closed after use to
        release network resources. To reuse the connection, it's required to
        call `response.release_conn()` explicitly.

        :param bucket_name: Name of the bucket.
        :param object_name: Object name in the bucket.
        :param offset: Start byte position of object data.
        :param length: Number of bytes of object data from offset.
        :param request_headers: Any additional headers to be added with GET
                                request.
        :param ssec: Server-side encryption customer key.
        :param version_id: Version-ID of the object.
        :param extra_query_params: Extra query parameters for advanced usage.
        :return: bytes."""
        ...

    def fget_object(
        self,
        bucket_name: str,
        object_name: str,
        file_path: str,
        request_headers: DictType | None = None,
        version_id: str | None = None,
        extra_query_params: DictType | None = None,
        tmp_file_path: str | None = None,
    ):
        """
        Downloads data of an object to file.

        :param bucket_name: Name of the bucket.
        :param object_name: Object name in the bucket.
        :param file_path: Name of file to download.
        :param request_headers: Any additional headers to be added with GET
                                request.
        :param version_id: Version-ID of the object.
        :param extra_query_params: Extra query parameters for advanced usage.
        :param tmp_file_path: Path to a temporary file.
        :return: Object information."""
        ...

    def put_object(
        self,
        bucket_name: str,
        object_name: str,
        data: BinaryIO,
        length: int,
        content_type: str = "application/octet-stream",
        metadata: DictType | None = None,
        part_size: int = 0,
        num_parallel_uploads: int = 3,
        tags: dict | None = None,
        legal_hold: bool = False,
    ) -> RemoteFileStorageWriteResult:
        """
        Uploads data from a stream to an object in a bucket.

        :param bucket_name: Name of the bucket.
        :param object_name: Object name in the bucket.
        :param data: An object having callable read() returning bytes object.
        :param length: Data size; -1 for unknown size and set valid part_size.
        :param content_type: Content type of the object.
        :param metadata: Any additional metadata to be uploaded along
            with your PUT request.
        :param part_size: Multipart part size.
        :param num_parallel_uploads: Number of parallel uploads.
        :param tags: :class:`Tags` for the object.
        :param legal_hold: Flag to set legal hold for the object.
        :return: :class:`ObjectWriteResult` object."""
        ...

    def fput_object(
        self,
        bucket_name: str,
        object_name: str,
        file_path: str,
        content_type: str = "application/octet-stream",
        metadata: DictType | None = None,
        part_size: int = 0,
        num_parallel_uploads: int = 3,
        tags: dict | None = None,
        legal_hold: bool = False,
    ) -> RemoteFileStorageWriteResult:
        """
        Uploads data from a file to an object in a bucket.

        :param bucket_name: Name of the bucket.
        :param object_name: Object name in the bucket.
        :param file_path: Name of file to upload.
        :param content_type: Content type of the object.
        :param metadata: Any additional metadata to be uploaded along
            with your PUT request.
        :param part_size: Multipart part size
        :param num_parallel_uploads: Number of parallel uploads.
        :param tags: :class:`Tags` for the object.
        :param legal_hold: Flag to set legal hold for the object.
        :return: :class:`RemoteFileStorageWriteResult` object."""
        ...
