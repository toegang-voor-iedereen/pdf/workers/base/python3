from typing import Optional, List, Literal, Annotated
from pydantic import BaseModel, Field
from .util.resource import Resource

from .definition import Definition


class DefinitionList(Resource, BaseModel):
    """https://spec.nldoc.nl/Resource/DefinitionList"""

    type: Literal["https://spec.nldoc.nl/Resource/DefinitionList"] = (
        "https://spec.nldoc.nl/Resource/DefinitionList"
    )
    children: Optional[Annotated[List[Definition], Field(default=[])]] = None
