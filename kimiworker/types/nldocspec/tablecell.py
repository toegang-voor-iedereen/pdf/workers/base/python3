from typing import Optional, List, Literal, Annotated, Union
from pydantic import BaseModel, Field
from .util.resource import Resource
from .paragraph import Paragraph
from .image import Image
from .heading import Heading
from .unorderedlist import UnorderedList
from .orderedlist import OrderedList
from .table import Table
from .definitionlist import DefinitionList
from .blockquotation import BlockQuotation

ChildrenOfTableCell = Union[
    Image,
    Heading,
    UnorderedList,
    OrderedList,
    Paragraph,
    Table,
    DefinitionList,
    BlockQuotation,
]
DiscriminatedChildrenOfTableCell = Annotated[
    ChildrenOfTableCell, Field(discriminator="type")
]


class TableCell(Resource, BaseModel):
    """https://spec.nldoc.nl/Resource/TableCell"""

    type: Literal["https://spec.nldoc.nl/Resource/TableCell"] = (
        "https://spec.nldoc.nl/Resource/TableCell"
    )
    children: Annotated[
        List[DiscriminatedChildrenOfTableCell],
        Field(default=[]),
    ]
    colspan: Optional[Annotated[int, Field(default=1)]] = None
    rowspan: Optional[Annotated[int, Field(default=1)]] = None
