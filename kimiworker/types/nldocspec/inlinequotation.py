from typing import List, Optional, Any, Literal, Annotated, Union
from pydantic import BaseModel, Field
from .util.resource import Resource
from .properties.cite import Cite

from .text import Text
from .link import Link

ChildrenOfInlineQuotation = Union[
    Text,
    Link,
]
DiscriminatedChildrenOfInlineQuotation = Annotated[
    ChildrenOfInlineQuotation,
    Field(discriminator="type"),
]


class InlineQuotation(Resource, BaseModel):
    """https://spec.nldoc.nl/Resource/InlineQuotation"""

    type: Literal["https://spec.nldoc.nl/Resource/InlineQuotation"] = (
        "https://spec.nldoc.nl/Resource/InlineQuotation"
    )
    cite: Optional[Annotated[Cite, Field(default=None)]] = None
    children: Optional[
        Annotated[
            List[DiscriminatedChildrenOfInlineQuotation],
            Field(default=[]),
        ]
    ] = None
