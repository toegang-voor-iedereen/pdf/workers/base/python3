from typing import Optional, List, Literal, Annotated, Union
from pydantic import BaseModel, Field
from .util.resource import Resource

from .paragraph import Paragraph
from .image import Image
from .heading import Heading
from .unorderedlist import UnorderedList
from .orderedlist import OrderedList
from .table import Table
from .definitionlist import DefinitionList
from .blockquotation import BlockQuotation

ChildrenOfDocument = Union[
    Image,
    Heading,
    UnorderedList,
    OrderedList,
    Paragraph,
    Table,
    DefinitionList,
    BlockQuotation,
]
DiscriminatedChildrenOfDocument = Annotated[
    ChildrenOfDocument,
    Field(discriminator="type"),
]


class Document(Resource, BaseModel):
    """https://spec.nldoc.nl/Resource/Document"""

    type: Literal["https://spec.nldoc.nl/Resource/Document"] = (
        "https://spec.nldoc.nl/Resource/Document"
    )
    cite: Optional[Annotated[str, Field(default=None)]] = None
    children: Optional[
        Annotated[
            List[DiscriminatedChildrenOfDocument],
            Field(default=[]),
        ]
    ] = None
