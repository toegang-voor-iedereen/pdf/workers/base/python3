from typing import Union, Annotated
from pydantic import TypeAdapter, Field

from .properties import *

from .properties.cite import Cite
from .properties.caption import Caption
from .properties.id import Id
from .properties.type import Type

from .util import *
from .util.resource import Resource
from .util.anyresource import AnyResource
from .util.descriptors import descriptors_by_uri

from .descriptors.descriptor import Descriptor
from .descriptors.anydescriptor import AnyDescriptor
from .descriptors.booleandescriptor import BooleanDescriptor
from .descriptors.integerdescriptor import IntegerDescriptor
from .descriptors.numberdescriptor import NumberDescriptor
from .descriptors.objectdescriptor import ObjectDescriptor
from .descriptors.stringdescriptor import StringDescriptor

from .blockquotation import BlockQuotation
from .definition import Definition
from .definitionlist import DefinitionList
from .document import Document
from .heading import Heading
from .image import Image
from .inlinequotation import InlineQuotation
from .link import Link
from .listitem import ListItem
from .orderedlist import OrderedList, OrderedListStyleType
from .paragraph import Paragraph
from .table import Table
from .tablecell import TableCell
from .tableheader import TableHeader, TableHeaderScope
from .tablerow import TableRow
from .text import Text, TextStyling
from .unorderedlist import UnorderedList, UnorderedListStyleType
