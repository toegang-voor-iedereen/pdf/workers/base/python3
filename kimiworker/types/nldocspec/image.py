from typing import Optional, Literal, Annotated
from pydantic import BaseModel, Field
from .util.resource import Resource
from .properties.caption import Caption


class Image(Resource, BaseModel):
    """https://spec.nldoc.nl/Resource/Image"""

    type: Literal["https://spec.nldoc.nl/Resource/Image"] = (
        "https://spec.nldoc.nl/Resource/Image"
    )
    alternativeText: Optional[Annotated[str, Field(default=None)]] = None
    caption: Annotated[Caption, Field(default=[])]
