from __future__ import annotations
from typing import Optional, List, Literal, Annotated, ForwardRef
from pydantic import BaseModel, Field
from .util.resource import Resource


class Definition(Resource, BaseModel):
    """https://spec.nldoc.nl/Resource/Definition"""

    type: Literal["https://spec.nldoc.nl/Resource/Definition"] = (
        "https://spec.nldoc.nl/Resource/Definition"
    )
    term: Annotated["List[Paragraph]", Field(min_length=1)]
    details: Annotated["List[Paragraph]", Field(min_length=1)]


from .paragraph import Paragraph
