from __future__ import annotations
from typing import Optional, List, Literal, Annotated, Union
from pydantic import BaseModel, Field
from .util.resource import Resource

ChildrenOfTableRow = Union["TableCell", "TableHeader"]
DiscriminatedChildrenOfTableRow = Annotated[
    ChildrenOfTableRow, Field(discriminator="type")
]


class TableRow(Resource, BaseModel):
    """https://spec.nldoc.nl/Resource/TableRow"""

    type: Literal["https://spec.nldoc.nl/Resource/TableRow"] = (
        "https://spec.nldoc.nl/Resource/TableRow"
    )
    children: Annotated[
        List[DiscriminatedChildrenOfTableRow],
        Field(default=[]),
    ]


from .tableheader import TableHeader
from .tablecell import TableCell
