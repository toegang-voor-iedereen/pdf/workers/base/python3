from typing import Union, Annotated
from pydantic import TypeAdapter, Field

from ..blockquotation import BlockQuotation
from ..definition import Definition
from ..definitionlist import DefinitionList
from ..document import Document
from ..heading import Heading
from ..image import Image
from ..inlinequotation import InlineQuotation
from ..link import Link
from ..listitem import ListItem
from ..orderedlist import OrderedList, OrderedListStyleType
from ..paragraph import Paragraph
from ..table import Table
from ..tablecell import TableCell
from ..tableheader import TableHeader, TableHeaderScope
from ..tablerow import TableRow
from ..text import Text, TextStyling
from ..unorderedlist import UnorderedList, UnorderedListStyleType

AnyResource = Union[
    BlockQuotation,
    Definition,
    DefinitionList,
    Document,
    Heading,
    Image,
    InlineQuotation,
    Link,
    ListItem,
    OrderedList,
    Paragraph,
    Table,
    TableCell,
    TableHeader,
    TableRow,
    Text,
    UnorderedList,
]
AnyResourceDiscriminated = Annotated[AnyResource, Field(..., discriminator="type")]
AnyResourceAdapter: TypeAdapter[AnyResource] = TypeAdapter(AnyResourceDiscriminated)

AnyResourceWithChildren = Union[
    BlockQuotation,
    DefinitionList,
    Document,
    Heading,
    ListItem,
    OrderedList,
    Paragraph,
    Table,
    TableCell,
    TableHeader,
    TableRow,
    UnorderedList,
]
