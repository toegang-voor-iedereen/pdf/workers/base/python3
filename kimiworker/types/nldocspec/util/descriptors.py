from __future__ import annotations
from pydantic import BaseModel
from typing import List, Union
from ..properties.type import Type
import re
from ....uri import kimi_uri, kimi_label_uri, kimi_attribute_uri
from ....utils.bbox import BoundingBox

PossibleDescriptorType = Union[str, float, int, bool, object]


def descriptors_for_properties(
    confidence: float | None,
    bounding_box: BoundingBox | None,
    labels: dict[str, float] = {},
    attributes: dict[str, PossibleDescriptorType] = {},
) -> List[AnyDescriptor]:
    descriptors = label_descriptors(labels) + attribite_descriptors(attributes)

    if confidence is not None:
        descriptors.append(confidence_descriptor(confidence))
    if bounding_box is not None:
        descriptors.append(bounding_box_descriptor(bounding_box))

    return descriptors


def label_descriptor(label: str, confidence: float) -> NumberDescriptor:
    return NumberDescriptor(uri=kimi_label_uri(label), value=confidence)


def label_descriptors(labels: dict[str, float]) -> List[NumberDescriptor]:
    return [
        label_descriptor(label=label, confidence=confidence)
        for (label, confidence) in labels.items()
    ]


def confidence_descriptor(confidence: float) -> AnyDescriptor:
    return create_descriptor_by_value(uri=kimi_uri("Confidence"), value=confidence)


def attribute_descriptor(key: str, value: PossibleDescriptorType) -> AnyDescriptor:
    uri = kimi_attribute_uri(key)
    return create_descriptor_by_value(uri=uri, value=value)


def attribite_descriptors(
    attributes: dict[str, PossibleDescriptorType]
) -> List[AnyDescriptor]:
    return [attribute_descriptor(key, value) for key, value in attributes.items()]


def bounding_box_descriptor(bbox: BoundingBox) -> ObjectDescriptor:
    return ObjectDescriptor(uri=kimi_uri("BoundingBox"), value=bbox)


def create_descriptor_by_value(
    uri: str, value: PossibleDescriptorType
) -> AnyDescriptor:
    match value:
        case str():
            descriptor = StringDescriptor(uri=uri, value=value)
        case float():
            descriptor = NumberDescriptor(uri=uri, value=value)
        case int():
            descriptor = IntegerDescriptor(uri=uri, value=value)
        case bool():
            descriptor = BooleanDescriptor(uri=uri, value=value)
        case object():
            descriptor = ObjectDescriptor(uri=uri, value=value)
        case _:
            raise TypeError(f"Unsupported type: {type(value)}")

    return descriptor


def descriptors_by_uri(descriptors: Descriptors, uri: str) -> List[AnyDescriptor]:
    return list(filter(lambda descriptor: descriptor.uri == uri, descriptors))


def descriptors_by_uri_regexp(
    descriptors: Descriptors, uri_regexp: str
) -> List[AnyDescriptor]:
    return list(
        filter(lambda descriptor: re.search(uri_regexp, descriptor.uri), descriptors)
    )


def descriptors_contain_uri(descriptors: list[NumberDescriptor], uri: str) -> bool:
    """Returns True if any of the descriptors contain a descriptor with the given uri"""
    return len(list(filter(lambda descriptor: descriptor.uri == uri, descriptors))) > 0


from ..descriptors.anydescriptor import AnyDescriptor
from ..descriptors.numberdescriptor import NumberDescriptor
from ..descriptors.stringdescriptor import StringDescriptor
from ..descriptors.integerdescriptor import IntegerDescriptor
from ..descriptors.booleandescriptor import BooleanDescriptor
from ..descriptors.objectdescriptor import ObjectDescriptor
from ..properties.descriptors import Descriptors
