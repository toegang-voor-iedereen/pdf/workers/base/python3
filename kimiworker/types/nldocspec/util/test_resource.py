import pytest
from .resource import Resource
from ..document import Document
from ..heading import Heading
from ..text import Text
from ..paragraph import Paragraph
from ..util.descriptors import label_descriptors


def test_resource_children_returns_empty_array_when_no_children_possible():
    resource = Text(
        id="edbcfd64-4b40-4f89-a006-aad32c0078de",
        text="Text without children",
        descriptors=[],
    )
    assert len(resource.get_children()) == 0


def test_resource_children_returns_empty_array_when_no_children_present():
    resource = Document(
        id="edbcfd64-4b40-4f89-a006-aad32c0078de",
        descriptors=[],
    )
    assert len(resource.get_children()) == 0


def test_document_children_returns_children():
    heading_text = Text(
        id="f9883f81-c6f8-4a42-9dfe-a51ab4891e3f", text="heading 1 text"
    )
    heading = Heading(
        id="cdbb6c3c-60a8-4460-a40d-6c204550a221", level=1, children=[heading_text]
    )
    paragraph_text = Text(
        id="b92fd30c-fd8d-47b8-8028-d9f49ca6c0e1", text="paragraph text"
    )
    paragraph = Paragraph(
        id="3a41045d-d5c5-4610-9780-154177e12692", children=[paragraph_text]
    )
    document = Document(
        id="c22882bf-aba3-4026-97dd-7a700e949a9a", children=[heading, paragraph]
    )
    assert len(document.get_children()) == 2
    assert len(heading.get_children()) == 1


def test_has_label_returns_true_if_found():
    document = Document(
        id="c22882bf-aba3-4026-97dd-7a700e949a9a",
        descriptors=[] + label_descriptors({"abc": 0.123, "def": 0.567}),
    )
    assert document.has_label("abc") is True


def test_has_label_returns_false_if_case_mismatch():
    document = Document(
        id="c22882bf-aba3-4026-97dd-7a700e949a9a",
        descriptors=[] + label_descriptors({"ABC": 0.123, "def": 0.567}),
    )
    assert document.has_label("abc") is False


def test_has_label_returns_false_if_not_found():
    document = Document(
        id="c22882bf-aba3-4026-97dd-7a700e949a9a",
        descriptors=[] + label_descriptors({"abc": 0.123, "def": 0.567}),
    )
    assert document.has_label("xyz") is False


def test_delete_label_deletes_the_label():
    document = Document(
        id="c22882bf-aba3-4026-97dd-7a700e949a9a",
        descriptors=[] + label_descriptors({"abc": 0.123, "def": 0.567}),
    )

    assert document.has_label("abc") is True
    assert document.has_label("def") is True

    # delete label abc
    document.delete_labels(label_descriptors({"abc": 0.4}))

    assert document.has_label("abc") is False
    assert document.has_label("def") is True
