from ..descriptors import StringDescriptor, IntegerDescriptor
from ..properties.descriptors import Descriptors

from .descriptors import descriptors_by_uri, descriptors_by_uri_regexp


def test_descriptors_by_uri__regexp_returns_correct_descriptors():
    """
    Tests whether descriptors_by_uri returns only the descriptors that match a given uri.
    """
    descriptors_base: Descriptors = [
        StringDescriptor(
            uri="http://api.example.com/matching_uri/foo",
            value="Matching Value",
        ),
        StringDescriptor(
            uri="http://api.example.com/non_matching_uri/foo",
            value="Non Matching Value",
        ),
        StringDescriptor(
            uri="http://api.example.com/matching_uri/123",
            value="Non Matching Value",
        ),
        IntegerDescriptor(
            uri="http://api.example.com/matching_uri/bar",
            value=1,
        ),
        IntegerDescriptor(
            uri="http://api.example.com/non_matching_uri/bar",
            value=-1,
        ),
        IntegerDescriptor(
            uri="http://api.example.com/matching_uri/123",
            value=-1,
        ),
    ]

    found_descriptors = descriptors_by_uri_regexp(
        descriptors_base, "http://api.example.com/matching_uri/[a-z]+"
    )

    assert (
        len(found_descriptors) == 2
    ), f"Expected 2 found descriptors, got {len(found_descriptors)}"

    assert (
        type(found_descriptors[0]) is StringDescriptor
    ), f"Expected one StringDescriptr, got {type(found_descriptors[0])}"

    assert (
        type(found_descriptors[1]) is IntegerDescriptor
    ), f"Expected one IntegerDescriptor, got {type(found_descriptors[1])}"


def test_descriptors_by_uri_returns_correct_descriptors():
    """
    Tests whether descriptors_by_uri returns only the descriptors that match a given uri.
    """
    descriptors_base: Descriptors = [
        StringDescriptor(
            uri="http://api.example.com/matching_uri",
            value="Matching Value",
        ),
        StringDescriptor(
            uri="http://api.example.com/non_matching_uri",
            value="Non Matching Value",
        ),
        IntegerDescriptor(
            uri="http://api.example.com/matching_uri",
            value=1,
        ),
        IntegerDescriptor(
            uri="http://api.example.com/non_matching_uri",
            value=-1,
        ),
    ]

    found_descriptors = descriptors_by_uri(
        descriptors_base, "http://api.example.com/matching_uri"
    )

    assert (
        len(found_descriptors) == 2
    ), f"Expected 2 found descriptors, got {len(found_descriptors)}"

    assert (
        type(found_descriptors[0]) is StringDescriptor
    ), f"Expected one StringDescriptr, got {type(found_descriptors[0])}"

    assert (
        type(found_descriptors[1]) is IntegerDescriptor
    ), f"Expected one IntegerDescriptor, got {type(found_descriptors[1])}"
