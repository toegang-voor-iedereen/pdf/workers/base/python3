from __future__ import annotations
import uuid

from typing import Annotated, Optional, Union
from itertools import chain
from pydantic import BaseModel, Field
from ..properties.id import Id
from ..properties.descriptors import Descriptors
from ..descriptors.anydescriptor import AnyDescriptor
from ..descriptors.booleandescriptor import BooleanDescriptor
from ..descriptors.integerdescriptor import IntegerDescriptor
from ..descriptors.numberdescriptor import NumberDescriptor
from ..descriptors.stringdescriptor import StringDescriptor
from ..descriptors.objectdescriptor import ObjectDescriptor
from .descriptors import (
    descriptors_by_uri,
    descriptors_by_uri_regexp,
    descriptors_contain_uri,
)
from ....uri import kimi_uri, kimi_attribute_uri, kimi_label_uri
from ....types import BoundingBox
from ..util.descriptors import PossibleDescriptorType, create_descriptor_by_value


class Resource(BaseModel):
    id: Annotated[
        Id,
        Field(min_length=36, max_length=36, default_factory=lambda: str(uuid.uuid4())),
    ]
    type: str
    descriptors: Optional[Annotated[Descriptors, Field(default=[])]] = None

    def get_children(self) -> list[AnyResource]:  # type: ignore - because of circular reference to Resource
        return getattr(self, "children", []) or []

    def certain_bounding_box(self) -> BoundingBox:
        return self.get_bounding_box() or {"top": 0, "left": 0, "right": 0, "bottom": 0}

    def get_bounding_box(self) -> BoundingBox | None:
        return bounding_box_from_resource(self)

    def has_label(self, label: str) -> bool:
        labels = self.descriptors_by_uri(kimi_label_uri(label))
        return len(labels) > 0

    def get_label_descriptors(self) -> list[NumberDescriptor]:
        label_descriptors = self.descriptors_by_uri_regexp("Label/.*")
        return [
            label for label in label_descriptors if isinstance(label, NumberDescriptor)
        ]

    def delete_labels(self, labels: list[NumberDescriptor]):
        """Removes the given labels from the resource's labels"""

        self.descriptors = list(
            filter(
                lambda descriptor: not descriptors_contain_uri(labels, descriptor.uri),
                self.descriptors or [],
            )
        )

    def set_labels(self, labels: list[NumberDescriptor]):
        """Sets the resource's labels to the given labels, removing any existing labels that are not in the given"""

        # Delete existing labels
        existing_label_descriptors = self.get_label_descriptors()
        self.delete_labels(existing_label_descriptors)

        # Set descriptors to current descriptors + new labels
        self.descriptors = (self.descriptors or []) + labels
        return

    def get_attribute(self, name: str) -> PossibleDescriptorType | None:
        uri = kimi_attribute_uri(name)
        descriptor = self.first_descriptor_with_uri(uri)
        return None if descriptor is None else descriptor.value

    def certain_attribute(
        self, name: str, value_if_none: PossibleDescriptorType
    ) -> PossibleDescriptorType:
        value = self.get_attribute(name)
        return value_if_none if value is None else value

    def set_attribute(self, name: str, value):
        uri = kimi_attribute_uri(name)
        self.set_descriptor(uri=uri, value=value)

    def set_attributes(self, values: dict[str, PossibleDescriptorType]):
        for key in values:
            self.set_attribute(name=key, value=values[key])

    def has_attribute(self, name: str) -> bool:
        labels = self.descriptors_by_uri(kimi_attribute_uri(name))
        return len(labels) > 0

    def set_descriptor(self, uri: str, value: PossibleDescriptorType):
        if self.descriptors is not None:
            existing_descriptors = self.descriptors_by_uri(uri)
            for existing_descriptor in existing_descriptors:
                self.descriptors.remove(existing_descriptor)

        self.add_descriptor(uri=uri, value=value)

    def add_descriptor(self, uri: str, value: PossibleDescriptorType):
        descriptor = create_descriptor_by_value(uri=uri, value=value)

        if self.descriptors is None:
            self.descriptors = [descriptor]
        else:
            self.descriptors.append(descriptor)

    def descriptors_by_uri(self, uri: str) -> Descriptors:
        return (
            []
            if self.descriptors is None or self.descriptors == []
            else descriptors_by_uri(self.descriptors, uri)
        )

    def descriptors_by_uri_regexp(self, uri_regexp: str) -> Descriptors:
        return (
            []
            if self.descriptors is None or self.descriptors == []
            else descriptors_by_uri_regexp(self.descriptors, uri_regexp)
        )

    def first_descriptor_with_uri(self, uri: str) -> AnyDescriptor | None:
        descriptors = self.descriptors_by_uri(uri)
        return descriptors[0] if len(descriptors) > 0 else None

    def value_for_uri(self, uri: str) -> PossibleDescriptorType | None:
        descriptor = self.first_descriptor_with_uri(uri)
        return None if descriptor is None else descriptor.value

    # boolean
    def boolean_descriptor_with_uri(self, uri: str) -> BooleanDescriptor | None:
        descriptor = self.first_descriptor_with_uri(uri)
        return descriptor if isinstance(descriptor, BooleanDescriptor) else None

    def boolean_value_for_uri(self, uri: str) -> bool | None:
        descriptor = self.boolean_descriptor_with_uri(uri)
        return None if descriptor is None else descriptor.value

    def certain_boolean_value_for_uri(self, uri: str, default: bool = False) -> bool:
        descriptor = self.boolean_descriptor_with_uri(uri)
        return (
            default
            if descriptor is None or descriptor.value is None
            else descriptor.value
        )

    # integer
    def integer_descriptor_with_uri(self, uri: str) -> IntegerDescriptor | None:
        descriptor = self.first_descriptor_with_uri(uri)
        return descriptor if isinstance(descriptor, IntegerDescriptor) else None

    def integer_value_for_uri(self, uri: str) -> int | None:
        descriptor = self.integer_descriptor_with_uri(uri)
        return None if descriptor is None else descriptor.value

    def certain_integer_value_for_uri(self, uri: str, default: int = 0) -> int:
        descriptor = self.integer_descriptor_with_uri(uri)
        return (
            default
            if descriptor is None or descriptor.value is None
            else descriptor.value
        )

    # number
    def number_descriptor_with_uri(self, uri: str) -> NumberDescriptor | None:
        descriptor = self.first_descriptor_with_uri(uri)
        return descriptor if isinstance(descriptor, NumberDescriptor) else None

    def number_value_for_uri(self, uri: str) -> float | None:
        descriptor = self.number_descriptor_with_uri(uri)
        return None if descriptor is None else descriptor.value

    def certain_number_value_for_uri(self, uri: str, default: float = 0.0) -> float:
        descriptor = self.number_descriptor_with_uri(uri)
        return (
            default
            if descriptor is None or descriptor.value is None
            else descriptor.value
        )

    # string
    def string_descriptor_with_uri(self, uri: str) -> StringDescriptor | None:
        descriptor = self.first_descriptor_with_uri(uri)
        return descriptor if isinstance(descriptor, StringDescriptor) else None

    def string_value_for_uri(self, uri: str) -> str | None:
        descriptor = self.string_descriptor_with_uri(uri)
        return None if descriptor is None else descriptor.value

    def certain_string_value_for_uri(self, uri: str, default: str = "") -> str:
        descriptor = self.string_descriptor_with_uri(uri)
        return (
            default
            if descriptor is None or descriptor.value is None
            else descriptor.value
        )

    # object
    def object_descriptor_with_uri(self, uri: str) -> ObjectDescriptor | None:
        descriptor = self.first_descriptor_with_uri(uri)
        return descriptor if isinstance(descriptor, ObjectDescriptor) else None

    def object_value_for_uri(self, uri: str) -> object | None:
        descriptor = self.object_descriptor_with_uri(uri)
        return None if descriptor is None else descriptor.value

    def certain_object_value_for_uri(self, uri: str, default: object = {}) -> object:
        descriptor = self.object_descriptor_with_uri(uri)
        return (
            default
            if descriptor is None or descriptor.value is None
            else descriptor.value
        )


def is_dict_type(dictionary: dict, key_type: type, value_type: type):
    keys_type = set(map(type, dictionary))
    values_type = set(type(dictionary.get(k)) for k in dictionary)
    return keys_type == {key_type} and values_type == {value_type}


def bounding_box_from_resource(resource: Resource) -> BoundingBox | None:
    if resource.descriptors is None:
        return None

    found_descriptors = descriptors_by_uri(
        resource.descriptors, kimi_uri("BoundingBox")
    )
    if len(found_descriptors) == 0:
        return None

    bbox_value = found_descriptors[0].value

    if type(bbox_value) is not dict or not is_dict_type(bbox_value, str, float):
        return None

    return {
        "top": bbox_value.get("top", 0),
        "left": bbox_value.get("left", 0),
        "bottom": bbox_value.get("bottom", 0),
        "right": bbox_value.get("right", 0),
    }


def merge_label_descriptors(
    current: list[NumberDescriptor],
    merge: list[list[NumberDescriptor]],
):
    """Merges into current labels if label not present or if it has a higher confidence value"""
    labels_dict = {current_label.uri: current_label for current_label in current}

    for label in chain.from_iterable(merge):
        name = label.uri
        confidence = label.value or 0
        if name not in labels_dict or confidence > (labels_dict[name].value or 0):
            labels_dict[name] = label

    return list(labels_dict.values())
