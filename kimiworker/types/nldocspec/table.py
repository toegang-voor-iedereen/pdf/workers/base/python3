from __future__ import annotations
from typing import Optional, List, Literal, Annotated
from pydantic import BaseModel, Field
from .util.resource import Resource
from .properties.caption import Caption


class Table(Resource, BaseModel):
    """https://spec.nldoc.nl/Resource/Table"""

    type: Literal["https://spec.nldoc.nl/Resource/Table"] = (
        "https://spec.nldoc.nl/Resource/Table"
    )
    children: Optional[Annotated[List[TableRow], Field(default=[])]] = None
    caption: Annotated[Caption, Field(default=[])]


from .tablerow import TableRow
