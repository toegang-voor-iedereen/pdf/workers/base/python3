from __future__ import annotations
from typing import Optional, List, Literal, Annotated, Union
from enum import Enum
from pydantic import BaseModel, Field
from .util.resource import Resource

ChildrenOfTableHeader = Union[
    "Image",
    "Heading",
    "UnorderedList",
    "OrderedList",
    "Paragraph",
    "Table",
    "DefinitionList",
    "BlockQuotation",
]
DiscriminatedChildrenOfTableHeader = Annotated[
    ChildrenOfTableHeader, Field(discriminator="type")
]


class TableHeaderScope(Enum):
    column = "column"
    row = "row"


class TableHeader(Resource, BaseModel):
    """https://spec.nldoc.nl/Resource/TableHeader"""

    type: Literal["https://spec.nldoc.nl/Resource/TableHeader"] = (
        "https://spec.nldoc.nl/Resource/TableHeader"
    )
    abbreviation: Optional[Annotated[str, Field(default=None)]] = None
    scope: Optional[Annotated[TableHeaderScope, Field(default=None)]] = None
    children: Annotated[
        List[DiscriminatedChildrenOfTableHeader],
        Field(default=[]),
    ]
    colspan: Optional[Annotated[int, Field(default=1, ge=1, le=1000)]] = None
    rowspan: Optional[Annotated[int, Field(default=1, ge=1, le=65534)]] = None


from .paragraph import Paragraph
from .image import Image
from .heading import Heading
from .unorderedlist import UnorderedList
from .orderedlist import OrderedList
from .table import Table
from .definitionlist import DefinitionList
from .blockquotation import BlockQuotation
