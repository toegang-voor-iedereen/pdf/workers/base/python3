from __future__ import annotations
from typing import Optional, List, Literal, Annotated
from enum import Enum
from pydantic import BaseModel, Field
from .util.resource import Resource


class UnorderedListStyleType(Enum):
    disc = "disc"
    circle = "circle"
    square = "square"


class UnorderedList(Resource, BaseModel):
    """https://spec.nldoc.nl/Resource/UnorderedList"""

    type: Literal["https://spec.nldoc.nl/Resource/UnorderedList"] = (
        "https://spec.nldoc.nl/Resource/UnorderedList"
    )
    styleType: Optional[
        Annotated[UnorderedListStyleType, UnorderedListStyleType.disc]
    ] = None
    children: Optional[Annotated[List[ListItem], Field(default=[])]] = None


from .listitem import ListItem
