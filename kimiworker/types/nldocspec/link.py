from typing import List, Optional, Literal, Annotated
from enum import Enum
from pydantic import BaseModel, Field
from .util.resource import Resource


class TextStyling(Enum):
    bold = "bold"
    italic = "italic"
    underline = "underline"
    strikethrough = "strikethrough"
    superscript = "superscript"
    subscript = "subscript"
    code = "code"


class Link(Resource, BaseModel):
    """https://spec.nldoc.nl/Resource/Link"""

    type: Literal["https://spec.nldoc.nl/Resource/Link"] = (
        "https://spec.nldoc.nl/Resource/Link"
    )
    text: Annotated[str, Field(min_length=1)]
    styling: Annotated[List[TextStyling], Field(default=[])]
    uri: Annotated[str, Field(min_length=1)]
    purpose: Optional[Annotated[str, Field(min_length=1)]] = None
