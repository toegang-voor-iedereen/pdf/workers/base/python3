from __future__ import annotations
from typing import Optional, List, Annotated, Literal, Union
from pydantic import BaseModel, Field
from .util.resource import Resource

from .properties.cite import Cite
from .properties.caption import Caption

ChildrenOfBlockQuotation = Union[
    "Paragraph",
    "Image",
    "Heading",
    "UnorderedList",
    "OrderedList",
    "Table",
    "DefinitionList",
]
DiscriminatedChildrenOfBlockQuotation = Annotated[
    ChildrenOfBlockQuotation,
    Field(discriminator="type"),
]


class BlockQuotation(Resource, BaseModel):
    """https://spec.nldoc.nl/Resource/BlockQuotation"""

    type: Literal["https://spec.nldoc.nl/Resource/BlockQuotation"] = (
        "https://spec.nldoc.nl/Resource/BlockQuotation"
    )
    cite: Optional[Annotated[Cite, Field(default=None)]] = None
    caption: Optional[Annotated[Caption, Field(default=None)]] = None
    children: Annotated[
        List[DiscriminatedChildrenOfBlockQuotation],
        Field(default=[]),
    ]


from .paragraph import Paragraph
from .image import Image
from .heading import Heading
from .unorderedlist import UnorderedList
from .orderedlist import OrderedList
from .table import Table
from .definitionlist import DefinitionList
