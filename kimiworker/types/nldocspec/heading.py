from typing import List, Optional, Literal, Annotated, Union
from pydantic import BaseModel, Field
from .util.resource import Resource
from .inlinequotation import InlineQuotation
from .image import Image
from .link import Link
from .text import Text

ChildrenOfHeading = Union[
    Text,
    Link,
    InlineQuotation,
    Image,
]
DiscriminatedChildrenOfHeading = Annotated[
    ChildrenOfHeading,
    Field(discriminator="type"),
]


class Heading(Resource, BaseModel):
    """https://spec.nldoc.nl/Resource/Heading"""

    type: Literal["https://spec.nldoc.nl/Resource/Heading"] = (
        "https://spec.nldoc.nl/Resource/Heading"
    )
    level: Annotated[int, Field(ge=1)]
    children: Optional[
        Annotated[
            List[
                Annotated[
                    Text | Link | InlineQuotation | Image, Field(discriminator="type")
                ]
            ],
            Field(default=[]),
        ]
    ] = None
