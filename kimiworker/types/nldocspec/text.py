from typing import List, Optional, Literal, Annotated
from enum import Enum
from pydantic import BaseModel, Field
from .util.resource import Resource


class TextStyling(Enum):
    bold = "bold"
    italic = "italic"
    underline = "underline"
    strikethrough = "strikethrough"
    superscript = "superscript"
    subscript = "subscript"
    code = "code"


class Text(Resource, BaseModel):
    """https://spec.nldoc.nl/Resource/Text"""

    type: Literal["https://spec.nldoc.nl/Resource/Text"] = (
        "https://spec.nldoc.nl/Resource/Text"
    )
    text: Annotated[str, Field(min_length=1)]
    styling: Annotated[List[TextStyling], Field(default=[])] = []
