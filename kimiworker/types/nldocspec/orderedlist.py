from __future__ import annotations
from typing import Optional, List, Literal, Annotated, Union
from enum import Enum
from pydantic import BaseModel, Field
from .util.resource import Resource


class OrderedListStyleType(Enum):
    decimal = "decimal"
    lower_alpha = "lower-alpha"
    upper_alpha = "upper-alpha"
    lower_roman = "lower-roman"
    upper_roman = "upper-roman"


class OrderedList(Resource, BaseModel):
    """https://spec.nldoc.nl/Resource/OrderedList"""

    type: Literal["https://spec.nldoc.nl/Resource/OrderedList"] = (
        "https://spec.nldoc.nl/Resource/OrderedList"
    )
    start: Optional[Annotated[int, Field(default=1)]] = None
    reversed: Optional[Annotated[bool, Field(default=False)]] = None
    styleType: Optional[
        Annotated[OrderedListStyleType, Field(default=OrderedListStyleType.decimal)]
    ] = None
    children: Optional[Annotated[List[ListItem], Field(default=[])]] = None


from .listitem import ListItem
