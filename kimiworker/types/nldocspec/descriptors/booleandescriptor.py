from .descriptor import Descriptor
from pydantic import BaseModel
from typing import Optional, Literal


class BooleanDescriptor(Descriptor, BaseModel):
    type: Literal["https://spec.nldoc.nl/Resource/BooleanDescriptor"] = (
        "https://spec.nldoc.nl/Resource/BooleanDescriptor"
    )
    uri: str
    value: Optional[bool] = None
