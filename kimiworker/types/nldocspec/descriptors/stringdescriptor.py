from .descriptor import Descriptor
from pydantic import BaseModel
from typing import Optional, Literal


class StringDescriptor(Descriptor, BaseModel):
    type: Literal["https://spec.nldoc.nl/Resource/StringDescriptor"] = (
        "https://spec.nldoc.nl/Resource/StringDescriptor"
    )
    uri: str
    value: Optional[str] = None
