from pydantic import BaseModel
from ..properties.type import Type


class Descriptor(BaseModel):
    type: Type
    uri: str
    pass
