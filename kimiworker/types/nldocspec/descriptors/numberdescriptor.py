from .descriptor import Descriptor
from pydantic import BaseModel
from typing import Optional, Literal


class NumberDescriptor(Descriptor, BaseModel):
    type: Literal["https://spec.nldoc.nl/Resource/NumberDescriptor"] = (
        "https://spec.nldoc.nl/Resource/NumberDescriptor"
    )
    uri: str
    value: Optional[float] = None
