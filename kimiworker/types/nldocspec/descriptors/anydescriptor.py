from typing import Annotated
from pydantic import Field, TypeAdapter

from .booleandescriptor import BooleanDescriptor
from .integerdescriptor import IntegerDescriptor
from .numberdescriptor import NumberDescriptor
from .objectdescriptor import ObjectDescriptor
from .stringdescriptor import StringDescriptor

AnyDescriptor = (
    BooleanDescriptor
    | IntegerDescriptor
    | NumberDescriptor
    | ObjectDescriptor
    | StringDescriptor
)

AnyDescriptorDiscriminated = Annotated[AnyDescriptor, Field(..., discriminator="type")]
AnyDescriptorAdapter: TypeAdapter[AnyDescriptor] = TypeAdapter(
    AnyDescriptorDiscriminated
)
