from .descriptor import Descriptor
from pydantic import BaseModel
from typing import Optional, Dict, Any, Literal


class ObjectDescriptor(Descriptor, BaseModel):
    type: Literal["https://spec.nldoc.nl/Resource/ObjectDescriptor"] = (
        "https://spec.nldoc.nl/Resource/ObjectDescriptor"
    )
    uri: str
    value: Optional[object] = None
