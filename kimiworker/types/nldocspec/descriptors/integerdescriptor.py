from .descriptor import Descriptor
from pydantic import BaseModel
from typing import Optional, Literal


class IntegerDescriptor(Descriptor, BaseModel):
    type: Literal["https://spec.nldoc.nl/Resource/IntegerDescriptor"] = (
        "https://spec.nldoc.nl/Resource/IntegerDescriptor"
    )
    uri: str
    value: Optional[int] = None
