from typing import Union
from .descriptor import Descriptor
from .booleandescriptor import BooleanDescriptor
from .integerdescriptor import IntegerDescriptor
from .numberdescriptor import NumberDescriptor
from .objectdescriptor import ObjectDescriptor
from .stringdescriptor import StringDescriptor
from .anydescriptor import AnyDescriptor
