from __future__ import annotations
from typing import List, Optional, Literal, Annotated, Union
from pydantic import BaseModel, Field
from .util.resource import Resource
from .inlinequotation import InlineQuotation
from .link import Link
from .text import Text

ChildrenOfParagraph = Union["Text", "Link", "InlineQuotation", "Image"]
DiscriminatedChildrenOfParagraph = Annotated[
    ChildrenOfParagraph, Field(discriminator="type")
]


class Paragraph(Resource, BaseModel):
    """https://spec.nldoc.nl/Resource/Paragraph"""

    type: Literal["https://spec.nldoc.nl/Resource/Paragraph"] = (
        "https://spec.nldoc.nl/Resource/Paragraph"
    )
    children: Optional[
        Annotated[
            List[DiscriminatedChildrenOfParagraph,],
            Field(default=[]),
        ]
    ] = None


from .image import Image
