from __future__ import annotations
from typing import Optional, List, Literal, Annotated, Union
from pydantic import BaseModel, Field
from .util.resource import Resource
from .paragraph import Paragraph

ChildrenOfListItem = Union["OrderedList", "UnorderedList", "Paragraph"]
DiscriminatedChildrenOfListItem = Annotated[
    ChildrenOfListItem, Field(discriminator="type")
]


class ListItem(Resource, BaseModel):
    """https://spec.nldoc.nl/Resource/ListItem"""

    type: Literal["https://spec.nldoc.nl/Resource/ListItem"] = (
        "https://spec.nldoc.nl/Resource/ListItem"
    )
    children: Optional[
        Annotated[
            List[DiscriminatedChildrenOfListItem],
            Field(default=[]),
        ]
    ] = None


from .unorderedlist import UnorderedList
from .orderedlist import OrderedList
