from __future__ import annotations
from typing import List
from ..descriptors.anydescriptor import AnyDescriptor

"""https://spec.nldoc.nl/Property/Descriptors"""
Descriptors = List[AnyDescriptor]
