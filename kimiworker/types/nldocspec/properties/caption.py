from __future__ import annotations
from typing import List

"""https://spec.nldoc.nl/Property/Caption"""
Caption = List["Paragraph"]

from ..paragraph import Paragraph
