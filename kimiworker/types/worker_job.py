from typing import TypedDict


class WorkerJob(TypedDict):
    recordId: str
    bucketName: str
    filename: str
    attributes: dict
