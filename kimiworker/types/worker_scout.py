from typing import TypedDict


class WorkerScout(TypedDict):
    reason: str
    timestamp: str
