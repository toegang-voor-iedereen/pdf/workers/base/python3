from typing import Protocol
from minio import Minio
from ..kimi_logger import KimiLogger
from .publish_method import PublishMethod
from .worker_job import WorkerJob
from .remote_file_storage import RemoteFileStorageInterface


class JobHandler(Protocol):
    def __call__(
        self,
        log: KimiLogger,
        job: WorkerJob,
        job_id: str,
        local_file_path: str,
        remote_file_storage: RemoteFileStorageInterface,
        publish: PublishMethod,
    ): ...
