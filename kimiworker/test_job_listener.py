import json
from unittest.mock import MagicMock, call, patch
import signal

import pika
import pika.exceptions
import pytest

from kimiworker.config import Config
from kimiworker.types.job_handler import JobHandler

from .job_listener import JobListener
from .kimi_logger import KimiLogger


@pytest.fixture(autouse=True)
def mock_env(monkeypatch):
    """Set up test environment variables. autouse=True makes this run for all tests"""
    monkeypatch.setenv("AMQP_HOST", "localhost")
    monkeypatch.setenv("AMQP_PORT", "1234")
    monkeypatch.setenv("AMQP_USER", "test-user")
    monkeypatch.setenv("AMQP_PASS", "test-pass")
    monkeypatch.setenv("EXCHANGE", "test-exchange")
    monkeypatch.setenv("HOSTNAME", "test-host")


@pytest.fixture
def job_handler_mock():
    return MagicMock(spec=JobHandler)


@pytest.fixture
@patch("kimiworker.job_listener.ConnectionManager")
def listener(mock_connection_manager, job_handler_mock, request):
    config = Config()
    logger = KimiLogger()
    minio = MagicMock()

    mock_channel = MagicMock()
    mock_connection_manager.return_value.get_channel.return_value = mock_channel

    listener_instance = JobListener(
        logger,
        minio,
        job_handler_mock,
        "test-queue",
        hostname="x",
        amqp_config=config.amqp,
    )

    def cleanup():
        if listener_instance.connection_manager:
            listener_instance.connection_manager.close()
        if listener_instance.dispatcher_connection_manager:
            listener_instance.dispatcher_connection_manager.close()

    request.addfinalizer(cleanup)

    return listener_instance


def test_initialization(listener):
    assert listener.amqp_host == "localhost"
    assert listener.amqp_port == "1234"
    assert listener.amqp_username == "test-user"
    assert listener.amqp_password == "test-pass"
    assert listener.exchange_prefix == "test-exchange"
    assert isinstance(listener.logger, KimiLogger)
    assert listener.queue_name == "test-queue"


def test_create_connection(listener):
    listener.setup_main_connection(number_of_concurrent_jobs=1)

    assert listener.connection_manager.get_channel.call_count == 3
    assert listener.channel is not None
    assert listener.publishing_channel is not None


def test_declare_base_queue(listener):
    listener.channel = MagicMock()
    listener.declare_base_queue()
    listener.channel.queue_declare.assert_called_once_with(
        queue="test-queue", durable=True, arguments={"x-queue-type": "quorum"}
    )


def test_declare_partitioned_queues(listener):
    listener.channel = MagicMock()

    listener.declare_partitioned_queues()
    expected_calls = [
        call(
            queue=f"test-queue_part_{i}",
            durable=True,
            arguments={"x-queue-type": "quorum"},
        )
        for i in range(listener.num_partitions)
    ]
    listener.channel.queue_declare.assert_has_calls(expected_calls, any_order=True)
    assert listener.channel.queue_declare.call_count == listener.num_partitions


def test_declare_exchanges(listener):
    listener.channel = MagicMock()
    listener.declare_exchanges()

    expected_exchange_names = [
        listener.health_exchange,
        listener.jobs_exchange,
        listener.results_exchange,
    ]
    actual_calls = [
        call_args[1]["exchange"]
        for call_args in listener.channel.exchange_declare.call_args_list
    ]
    assert set(actual_calls) == set(expected_exchange_names)


def test_bind_base_queue(listener):
    mock_temp_channel = MagicMock()
    listener.connection_manager.get_channel.return_value.__enter__.return_value = (
        mock_temp_channel
    )

    with patch.object(listener.logger, "info") as mock_logger:
        listener.bind_base_queue()

        mock_logger.assert_any_call(
            f"Binding exchange {listener.jobs_exchange} to queue {listener.queue_name} "
            f"with routing key '{listener.jobs_routing_key}'"
        )
        mock_logger.assert_any_call(
            f"Binding exchange {listener.health_exchange} to queue {listener.queue_name} "
            f"with routing key '{listener.health_routing_key}'"
        )

    mock_temp_channel.queue_bind.assert_any_call(
        listener.queue_name, listener.jobs_exchange, listener.jobs_routing_key
    )
    mock_temp_channel.queue_bind.assert_any_call(
        listener.queue_name, listener.health_exchange, listener.health_routing_key
    )


def test_send_heartbeat(listener):
    mock_channel = MagicMock()

    with patch.object(listener, "report_availability") as mock_report_availability:
        listener.send_heartbeat(channel=mock_channel)

        mock_report_availability.assert_called_once_with(
            mock_channel, "active", "Heartbeat"
        )

        listener.connection_manager.connection.add_callback_threadsafe.assert_called_once()


def test_stop_heartbeat(listener):
    real_connection = listener.connection_manager.connection
    mock_connection = MagicMock()
    listener.connection_manager.connection = mock_connection
    mock_connection.is_open = True

    listener._heartbeat_timeout_id = 12345

    listener.stop_heartbeat()

    assert listener._heartbeat_timeout_id is None

    mock_connection.add_callback_threadsafe.assert_called_once()
    mock_connection.remove_timeout.assert_called_once_with(12345)

    listener.connection_manager.connection = real_connection


def test_handle_message(listener):
    listener.setup_main_connection(number_of_concurrent_jobs=1)

    mock_method = MagicMock(delivery_tag=123, routing_key="test-queue.job-1234")
    mock_properties = pika.BasicProperties(
        content_type="application/json", headers={"x-trace-id": "1234"}
    )

    with patch.object(listener, "handle_job_message") as mock_handle_job:
        listener.handle_message(
            listener.channel, mock_method, mock_properties, b'{"recordId": "1"}'
        )
        mock_handle_job.assert_called_once()

    listener.channel.basic_nack.assert_not_called()


def test_routing_key_processing(listener, job_handler_mock):
    listener.setup_main_connection(number_of_concurrent_jobs=1)
    listener.minio = MagicMock()
    listener.job_handler = job_handler_mock

    job_data = json.dumps(
        {"recordId": "1", "bucketName": "test-bucket", "filename": "file.jpg"}
    ).encode()
    mock_method = MagicMock(delivery_tag=123, routing_key="unknown-routing-key")
    mock_properties = pika.BasicProperties(
        content_type="application/json", headers={"x-trace-id": "1234"}
    )
    listener.handle_message(listener.channel, mock_method, mock_properties, job_data)

    listener.channel.basic_nack.assert_called()


def test_scout_processing(listener):
    listener.setup_main_connection(number_of_concurrent_jobs=1)

    mock_channel = MagicMock()
    mock_logger = MagicMock()

    test_reason = "Unit Testing"
    body = json.dumps({"reason": test_reason, "timestamp": ""}).encode()

    listener.handle_station_scout(
        channel=mock_channel,
        body=body,
        logger=mock_logger,
    )

    mock_channel.basic_publish.assert_called_once()

    call_args = mock_channel.basic_publish.call_args
    sent_message = json.loads(call_args[0][2])

    assert test_reason in sent_message["reason"]
    assert f"Answering Worker Scout: {test_reason}" == sent_message["reason"]


def test_job_processing(listener, job_handler_mock):
    listener.setup_main_connection(number_of_concurrent_jobs=1)
    listener.minio = MagicMock()
    listener.job_handler = job_handler_mock

    with patch.object(listener, "publish_job_ack"):
        with patch.object(listener, "handle_job"):
            job_data = json.dumps(
                {"recordId": "1", "bucketName": "test-bucket", "filename": "file.jpg"}
            ).encode()
            mock_method = MagicMock(delivery_tag=123, routing_key="test-queue_part_1")
            mock_properties = pika.BasicProperties(
                content_type="application/json", headers={"x-trace-id": "1234"}
            )

            listener.handle_message(
                listener.channel, mock_method, mock_properties, job_data
            )

            listener.channel.basic_ack.assert_called_once_with(123)


def test_handle_job_file_download_error(listener):
    listener.setup_main_connection(number_of_concurrent_jobs=1)

    listener.minio = MagicMock()
    listener.minio.bucket_exists.return_value = False

    logger = KimiLogger()
    result = listener.download_job_file(
        logger, "nonexistent-bucket", "file.jpg", "/fakepath/file.jpg"
    )
    assert result is False


def test_message_distribution(listener):
    listener.setup_main_connection(number_of_concurrent_jobs=1)

    uid1 = "test-uid-1"
    queue1 = listener.get_partitioned_queue_name(uid1)
    for _ in range(5):
        assert listener.get_partitioned_queue_name(uid1) == queue1

    uid2 = "test-uid-2"
    queue2 = listener.get_partitioned_queue_name(uid2)
    assert queue1 != queue2 or listener.num_partitions == 1

    counts = {}
    num_samples = 1000
    for i in range(num_samples):
        uid = f"test-uid-{i}"
        queue = listener.get_partitioned_queue_name(uid)
        counts[queue] = counts.get(queue, 0) + 1

    for queue_name in listener.partitioned_queue_names:
        assert queue_name in counts, f"Queue {queue_name} was never selected!"
        assert counts[queue_name] > 0, f"Queue {queue_name} had zero counts!"


def test_register_interrupt_handler(listener):
    with patch("signal.signal") as mock_signal:
        listener._JobListener__register_interrupt_handler()

        mock_signal.assert_any_call(signal.SIGINT, mock_signal.call_args[0][1])
        mock_signal.assert_any_call(signal.SIGTERM, mock_signal.call_args[0][1])
        assert mock_signal.call_count == 2


def test_interrupt_handler_functionality(listener):
    listener.stop_heartbeat = MagicMock()
    listener.dispatcher_thread = MagicMock()
    listener.report_interrupted = MagicMock()
    listener.stop_dispatcher_event = MagicMock()

    original_conn_manager = listener.connection_manager
    original_disp_conn_manager = listener.dispatcher_connection_manager
    listener.connection_manager = MagicMock()
    listener.dispatcher_connection_manager = MagicMock()

    with patch("signal.signal") as mock_signal:
        listener._JobListener__register_interrupt_handler()
        handler_func = mock_signal.call_args[0][1]

    with patch("sys.exit") as mock_exit:
        handler_func(signal.SIGTERM, None)

        listener.stop_heartbeat.assert_called_once()
        listener.stop_dispatcher_event.set.assert_called_once()
        listener.dispatcher_thread.join.assert_called_once()
        listener.report_interrupted.assert_called_once()
        listener.connection_manager.close.assert_called_once()
        listener.dispatcher_connection_manager.close.assert_called_once()
        mock_exit.assert_called_once_with(0)

    listener.connection_manager = original_conn_manager
    listener.dispatcher_connection_manager = original_disp_conn_manager


@pytest.mark.parametrize("signal_type", [signal.SIGINT, signal.SIGTERM])
def test_interrupt_handler_with_different_signals(listener, signal_type):
    original_conn_manager = listener.connection_manager
    original_disp_conn_manager = listener.dispatcher_connection_manager
    original_logger = listener.logger

    listener.stop_heartbeat = MagicMock()
    listener.dispatcher_thread = MagicMock()
    listener.report_interrupted = MagicMock()
    listener.stop_dispatcher_event = MagicMock()

    listener.connection_manager = MagicMock()
    listener.dispatcher_connection_manager = MagicMock()

    interrupt_logger = MagicMock()
    listener.logger = MagicMock()
    listener.logger.child = MagicMock(return_value=interrupt_logger)

    with patch("signal.signal") as mock_signal:
        listener._JobListener__register_interrupt_handler()
        handler_func = mock_signal.call_args[0][1]

    with patch("sys.exit") as mock_exit:
        handler_func(signal_type, None)

        expected_reason = f"Received interrupt signal: {signal_type}"
        listener.report_interrupted.assert_called_once()
        call_args = listener.report_interrupted.call_args
        assert call_args[1]["reason"] == expected_reason
        mock_exit.assert_called_once_with(0)

    listener.logger = original_logger
    listener.connection_manager = original_conn_manager
    listener.dispatcher_connection_manager = original_disp_conn_manager


def test_interrupt_handler_logs_events(listener):
    listener.stop_heartbeat = MagicMock()
    listener.dispatcher_thread = MagicMock()
    listener.report_interrupted = MagicMock()
    listener.stop_dispatcher_event = MagicMock()

    original_conn_manager = listener.connection_manager
    original_disp_conn_manager = listener.dispatcher_connection_manager
    listener.connection_manager = MagicMock()
    listener.dispatcher_connection_manager = MagicMock()

    original_logger = listener.logger
    interrupt_logger = MagicMock()
    listener.logger = MagicMock()
    listener.logger.child = MagicMock(return_value=interrupt_logger)

    with patch("signal.signal") as mock_signal:
        listener._JobListener__register_interrupt_handler()
        handler_func = mock_signal.call_args[0][1]

    with patch("sys.exit") as mock_exit:
        handler_func(signal.SIGINT, None)

        listener.logger.child.assert_called_once_with({"phase": "interrupt"})

        interrupt_logger.warning.assert_called_once()
        assert "Received interrupt signal" in interrupt_logger.warning.call_args[0][0]

        expected_log_calls = [
            call("Waiting for dispatcher thread to terminate..."),
            call("Dispatcher thread terminated."),
            call("Closing channels and connections"),
            call("Exiting"),
        ]
        interrupt_logger.info.assert_has_calls(expected_log_calls, any_order=False)
        mock_exit.assert_called_once_with(0)

    listener.logger = original_logger
    listener.connection_manager = original_conn_manager
    listener.dispatcher_connection_manager = original_disp_conn_manager


def test_interrupt_handler_no_dispatcher_thread(listener):
    original_dispatcher_thread = listener.dispatcher_thread
    original_conn_manager = listener.connection_manager
    original_disp_conn_manager = listener.dispatcher_connection_manager
    original_logger = listener.logger

    listener.dispatcher_thread = None
    listener.stop_heartbeat = MagicMock()
    listener.report_interrupted = MagicMock()
    listener.stop_dispatcher_event = MagicMock()

    listener.connection_manager = MagicMock()
    listener.dispatcher_connection_manager = MagicMock()

    interrupt_logger = MagicMock()
    listener.logger = MagicMock()
    listener.logger.child = MagicMock(return_value=interrupt_logger)

    with patch("signal.signal") as mock_signal:
        listener._JobListener__register_interrupt_handler()
        handler_func = mock_signal.call_args[0][1]

    with patch("sys.exit") as mock_exit:
        handler_func(signal.SIGINT, None)

        listener.stop_heartbeat.assert_called_once()
        listener.stop_dispatcher_event.set.assert_called_once()
        listener.report_interrupted.assert_called_once()
        listener.connection_manager.close.assert_called_once()
        listener.dispatcher_connection_manager.close.assert_called_once()
        mock_exit.assert_called_once_with(0)

    listener.dispatcher_thread = original_dispatcher_thread
    listener.logger = original_logger
    listener.connection_manager = original_conn_manager
    listener.dispatcher_connection_manager = original_disp_conn_manager


@patch("kimiworker.job_listener.ConnectionManager")
def test_start_method_basic_functionality(mock_connection_manager, listener):
    mock_channel = MagicMock()
    mock_connection_manager.return_value.get_channel.return_value = mock_channel

    with patch.object(listener, "setup_main_connection") as mock_setup_main:
        with patch.object(listener, "start_listening") as mock_start_listening:
            with patch("threading.Thread") as mock_thread:
                mock_thread_instance = MagicMock()
                mock_thread.return_value = mock_thread_instance

                listener.start()

                mock_setup_main.assert_called_once_with(1)
                mock_thread.assert_called_once()
                assert (
                    mock_thread.call_args[1]["target"]
                    == listener.start_dispatcher_consumption
                )
                assert mock_thread_instance.daemon is True
                mock_thread_instance.start.assert_called_once()

                mock_start_listening.assert_called_once()


@patch("kimiworker.job_listener.ConnectionManager")
def test_start_method_with_custom_concurrent_jobs(mock_connection_manager, listener):
    mock_channel = MagicMock()
    mock_connection_manager.return_value.get_channel.return_value = mock_channel

    with patch.object(listener, "setup_main_connection") as mock_setup_main:
        with patch.object(listener, "start_listening"):
            with patch("threading.Thread") as mock_thread:
                mock_thread_instance = MagicMock()
                mock_thread.return_value = mock_thread_instance
                custom_concurrent_jobs = 5

                listener.start(number_of_concurrent_jobs=custom_concurrent_jobs)

                mock_setup_main.assert_called_once_with(custom_concurrent_jobs)
                assert listener.number_of_concurrent_jobs == custom_concurrent_jobs


@patch("kimiworker.job_listener.ConnectionManager")
def test_start_method_connection_error_handling(mock_connection_manager, listener):
    with patch.object(listener, "setup_main_connection") as mock_setup_main:
        with patch.object(listener, "start_listening"):
            with patch("threading.Thread"):
                mock_setup_main.side_effect = pika.exceptions.AMQPConnectionError(
                    "Connection failed"
                )

                with patch.object(listener.logger, "critical") as mock_critical:
                    with patch("sys.exit") as mock_exit:
                        listener.start()

                        mock_critical.assert_called_once()
                        mock_exit.assert_called_once_with(1)
