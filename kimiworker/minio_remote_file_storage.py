from typing import Dict, List, Tuple, BinaryIO
from builtins import bytes
from .types.remote_file_storage import (
    RemoteFileStorageInterface,
    DictType,
    RemoteFileStorageWriteResult,
)

from minio import Minio
from minio.commonconfig import Tags


def minio_tags_from_dict(tags: dict | None):
    minio_tags = Tags(for_object=True)
    if tags is not None:
        for i, (k, v) in enumerate(tags.items()):
            minio_tags[k] = v
    return minio_tags


class MinioRemoteFileStorage(RemoteFileStorageInterface):
    def __init__(self, minio: Minio):
        self.__minio = minio

    def get_object(
        self,
        bucket_name: str,
        object_name: str,
        offset: int = 0,
        length: int = 0,
        request_headers: Dict[str, str | List[str] | Tuple[str]] | None = None,
        version_id: str | None = None,
        extra_query_params: Dict[str, str | List[str] | Tuple[str]] | None = None,
    ) -> bytes:
        object = self.__minio.get_object(
            bucket_name=bucket_name,
            object_name=object_name,
            offset=offset,
            length=length,
            request_headers=request_headers,
            version_id=version_id,
            extra_query_params=extra_query_params,
        )

        return object.data

    def fget_object(
        self,
        bucket_name: str,
        object_name: str,
        file_path: str,
        request_headers: Dict[str, str | List[str] | Tuple[str]] | None = None,
        version_id: str | None = None,
        extra_query_params: Dict[str, str | List[str] | Tuple[str]] | None = None,
        tmp_file_path: str | None = None,
    ):
        return self.__minio.fget_object(
            bucket_name=bucket_name,
            object_name=object_name,
            file_path=file_path,
            request_headers=request_headers,
            version_id=version_id,
            extra_query_params=extra_query_params,
            tmp_file_path=tmp_file_path,
        )

    def put_object(
        self,
        bucket_name: str,
        object_name: str,
        data: BinaryIO,
        length: int,
        content_type: str = "application/octet-stream",
        metadata: DictType | None = None,
        part_size: int = 0,
        num_parallel_uploads: int = 3,
        tags: dict | None = None,
        legal_hold: bool = False,
    ) -> RemoteFileStorageWriteResult:
        result = self.__minio.put_object(
            bucket_name=bucket_name,
            object_name=object_name,
            data=data,
            length=length,
            content_type=content_type,
            metadata=metadata,
            part_size=part_size,
            num_parallel_uploads=num_parallel_uploads,
            tags=minio_tags_from_dict(tags),
            legal_hold=legal_hold,
        )
        return {
            "bucket_name": result.bucket_name,
            "object_name": result.object_name,
            "etag": result.etag,
            "version_id": result.version_id,
        }

    def fput_object(
        self,
        bucket_name: str,
        object_name: str,
        file_path: str,
        content_type: str = "application/octet-stream",
        metadata: DictType | None = None,
        part_size: int = 0,
        num_parallel_uploads: int = 3,
        tags: dict | None = None,
        legal_hold: bool = False,
    ) -> RemoteFileStorageWriteResult:
        result = self.__minio.fput_object(
            bucket_name=bucket_name,
            object_name=object_name,
            file_path=file_path,
            content_type=content_type,
            metadata=metadata,
            part_size=part_size,
            num_parallel_uploads=num_parallel_uploads,
            tags=minio_tags_from_dict(tags),
            legal_hold=legal_hold,
        )

        return {
            "bucket_name": result.bucket_name,
            "object_name": result.object_name,
            "etag": result.etag,
            "version_id": result.version_id,
        }
