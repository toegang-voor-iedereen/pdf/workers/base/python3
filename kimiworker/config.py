"""
Application configuration module that loads settings from environment variables.
Provides structured access to MinIO, AMQP and other worker configuration settings.
"""

from dataclasses import dataclass, field
import os
import sys


@dataclass
class MinioConfig:
    """MinIO configuration settings."""

    host: str = field(default="localhost")
    port: str = field(default="9000")
    access_key: str = field(default="")
    secret_key: str = field(default="")
    use_ssl: bool = field(default=True)

    def __post_init__(self):
        # Load all values first
        self.host = os.getenv("MINIO_HOST", self.host)
        self.port = os.getenv("MINIO_PORT", self.port)
        self.access_key = os.getenv("MINIO_ACCESS_KEY", self.access_key)
        self.secret_key = os.getenv("MINIO_SECRET_KEY", self.secret_key)
        self.use_ssl = os.getenv("MINIO_USE_SSL", str(self.use_ssl)).lower() == "true"

        # Now validate all required fields
        missing = []
        if not self.host:
            missing.append("MINIO_HOST")
        if not self.port:
            missing.append("MINIO_PORT")
        if not self.access_key:
            missing.append("MINIO_ACCESS_KEY")
        if not self.secret_key:
            missing.append("MINIO_SECRET_KEY")
        if missing:
            print(f"Missing environment values for MinIO: {missing}")
            sys.exit(1)


@dataclass
class AMQPConfig:
    """AMQP (Advanced Message Queuing Protocol) configuration settings."""

    host: str = field(default="localhost")
    port: str = field(default="5672")
    username: str = field(default="")
    password: str = field(default="")
    exchange: str = field(default="")
    heartbeat_interval: int = field(default=60)
    num_partitions: int = field(default=20)

    def __post_init__(self):
        self.host = os.getenv("AMQP_HOST", self.host)
        self.port = os.getenv("AMQP_PORT", self.port)
        self.username = os.getenv("AMQP_USER", self.username)
        self.password = os.getenv("AMQP_PASS", self.password)
        self.exchange = os.getenv("EXCHANGE", self.exchange)

        try:
            heartbeat = os.getenv("AMQP_HEARTBEAT_INTERVAL")
            if heartbeat:
                self.heartbeat_interval = int(heartbeat)
        except ValueError:
            pass

        try:
            num_partitions = os.getenv("AMQP_NUM_PARTITIONS")
            if num_partitions:
                self.num_partitions = int(num_partitions)
        except ValueError:
            pass

        missing = []
        if not self.host:
            missing.append("AMQP_HOST")
        if not self.port:
            missing.append("AMQP_PORT")
        if not self.username:
            missing.append("AMQP_USER")
        if not self.password:
            missing.append("AMQP_PASS")
        if not self.exchange:
            missing.append("EXCHANGE")
        if missing:
            print(f"Missing environment values for AMQP: {missing}")
            sys.exit(1)


@dataclass
class Config:
    """Main configuration class that aggregates all application settings."""

    minio: MinioConfig = field(default_factory=MinioConfig)
    amqp: AMQPConfig = field(default_factory=AMQPConfig)
    hostname: str = field(default="")

    def __post_init__(self):
        self.hostname = os.getenv("HOSTNAME", self.hostname)
        if not self.hostname:
            print("Missing environment value for HOSTNAME")
            sys.exit(1)
