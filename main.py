#!/usr/bin/env python
import os
import random
import sys
import time

import kimiworker
from kimiworker.kimi_logger import KimiLogger
from kimiworker.types import PublishMethod, WorkerJob
from kimiworker.types.remote_file_storage import RemoteFileStorageInterface


def mockedJobHandler(
    log: KimiLogger,
    job: WorkerJob,
    job_id: str,
    local_file_path: str,
    remote_file_storage: RemoteFileStorageInterface,
    publish: PublishMethod,
):
    log.info("Mocked job handler started")
    log.info(f"Record ID: {job.get('recordId')}")
    log.info(f"File Path: {local_file_path}")
    time.sleep(random.randint(5, 15))

    publish("stringResult", "application/pdf", success=True, confidence=100)


if __name__ == "__main__":
    try:
        worker = kimiworker.Worker(mockedJobHandler, "worker-base-python-dummy")
        worker.start(number_of_concurrent_jobs=1)

    except KeyboardInterrupt:
        print("\n")
        print("Got interruption signal. Exiting...\n")
        try:
            sys.exit(0)
        except SystemExit:
            os._exit(0)
