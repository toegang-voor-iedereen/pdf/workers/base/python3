import os
from unittest import mock

import pytest


@pytest.fixture(autouse=True)
def mock_settings_env_vars():
    with mock.patch.dict(
        os.environ,
        {
            "AMQP_HOST": "localhost",
            "AMQP_PORT": "1234",
            "AMQP_USER": "amqp-user",
            "AMQP_PASS": "amqp-pass",
            "EXCHANGE": "foo-exchange",
            "MINIO_HOST": "localhost",
            "MINIO_PORT": "1234",
            "MINIO_ACCESS_KEY": "minio-access-key",
            "MINIO_SECRET_KEY": "minio-secret-key",
        },
    ):
        yield
