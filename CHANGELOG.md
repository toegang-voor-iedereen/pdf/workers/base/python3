## [4.4.3](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/compare/4.4.2...4.4.3) (2025-01-20)


### Bug Fixes

* ecs compliant fields ([52e81c5](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/commit/52e81c583065a2f773769dbe7f85d808d76fee44))

## [4.4.2](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/compare/4.4.1...4.4.2) (2024-12-30)


### Bug Fixes

* rebuild ValidationFile model after assignment ([db473ed](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/commit/db473ed85cf17fa4533b5c50607151ce027c63db))

## [4.4.1](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/compare/4.4.0...4.4.1) (2024-12-17)


### Bug Fixes

* handle empty headers ([0857f3b](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/commit/0857f3bdde4fc933a0dcd6f82f5f4236d5389882))

# [4.4.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/compare/4.3.0...4.4.0) (2024-12-11)


### Features

* put config in separate file ([3679b85](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/commit/3679b853e7c5ca4c6593f0cae23764adeb0ff183))

# [4.3.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/compare/4.2.0...4.3.0) (2024-12-11)


### Features

* added put_object to RemoteFileStorageInterface ([b6f3cc3](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/commit/b6f3cc3a5d8038d343cd35a75ee97e5049f2a2e2))

# [4.2.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/compare/4.1.0...4.2.0) (2024-12-11)


### Features

* also expose put_object() ([c917de8](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/commit/c917de8d7018739c34c71f29a1ec12886832b184))

# [4.1.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/compare/4.0.0...4.1.0) (2024-12-11)


### Features

* use worker queue name is debug file name ([5bd6e98](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/commit/5bd6e981de93ffa06ec58feae17ea0a31aed326c))

# [4.0.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/compare/3.6.3...4.0.0) (2024-12-11)


### Bug Fixes

* actually expose the remote file storage classes ([9dbe2d2](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/commit/9dbe2d258e8a6078a7cca4b42870a64ef2a232ec))


### Features

* expose remote file storage via proxy class instead of passing Minio client ([d375f61](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/commit/d375f61e2c00d31143d34d8de690071b2d902fd9))


### BREAKING CHANGES

* MinIO client is no longer passed as argument into job handler

## [3.6.3](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/compare/3.6.2...3.6.3) (2024-12-10)


### Bug Fixes

* make use SSL configurable ([1df20ed](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/commit/1df20edb08cfad0b7dff20d717ab965a39f25d48))

## [3.6.2](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/compare/3.6.1...3.6.2) (2024-12-10)


### Bug Fixes

* exit worker after 10 retries ([91b1af2](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/commit/91b1af21ff8c2456300a58d5b098ca01a0116339))

## [3.6.1](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/compare/3.6.0...3.6.1) (2024-11-27)


### Bug Fixes

* don't call is_open - read it ([c320582](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/commit/c320582a7c365adeeb11a9fd58330a5ba1e539bc))

# [3.6.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/compare/3.5.0...3.6.0) (2024-11-19)


### Features

* add job file to minio debug output ([ba43aa9](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/commit/ba43aa9bc65b7868ea746fa05488d815c41fca3d))

# [3.5.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/compare/3.4.0...3.5.0) (2024-11-06)


### Bug Fixes

* removed new test ([a5ee873](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/commit/a5ee87302e9919a6f08d7688c6f504e8c3c9c6ec))
* removed reference to nldocspec utils ([9ee95df](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/commit/9ee95df39373bb1d784ab8279a41bad8609c6c42))
* restored ContentDefinition utils ([34c2ade](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/commit/34c2adef93812203fd10f0442ce496fa30ce17e2))
* temp removed nldocspec resource tools ([c8615e0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/commit/c8615e0dac2e135515f4b68fe9951bb4133441c4))


### Features

* restored ContentDefinition ([39ada3d](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/commit/39ada3d59e6e31d65c5b390a95dfc6416283e6c1))

# [3.4.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/compare/3.3.1...3.4.0) (2024-11-06)

* restored ContentDefinition ([39ada3d](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/commit/39ada3d59e6e31d65c5b390a95dfc6416283e6c1))

## [3.3.1](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/compare/3.3.0...3.3.1) (2024-11-05)


### Bug Fixes

* don't check SSL certificate for now ([133829f](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/commit/133829fd64ce61346e77f8f152001d18a53cf732))

# [3.3.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/compare/3.2.0...3.3.0) (2024-11-05)


### Features

* connect to MinIO over HTTPS ([22141fe](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/commit/22141fe9f2e1ce9e126ff33bc54c96fffd084a24))

# [3.2.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/compare/3.1.0...3.2.0) (2024-10-29)


### Features

* added children definitions for resources ([6a63cd9](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/commit/6a63cd94d5c4fd63efecf453af893c11a5b7c3e5))

# [3.1.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/compare/3.0.1...3.1.0) (2024-10-28)


### Features

* added Resource.value_for_uri() method ([c78c078](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/commit/c78c07837822caad8b2eea6088a2695774bded07))

## [3.0.1](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/compare/3.0.0...3.0.1) (2024-10-23)


### Bug Fixes

* delete_label now actually deletes the correct labels ([a11cb20](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/commit/a11cb2001b9f13ebaa90b2a77c94faac75f87fa4))

# [3.0.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/compare/2.15.0...3.0.0) (2024-10-23)


### Features

* added descriptor utils, removed circular references ([7cc69dd](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/commit/7cc69ddffbd297b17dca3118075d5f54f61b8900))
* added various utilities for descriptors and resource labels ([a48fe54](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/commit/a48fe5438f82e021abd30eeb0f4abc6204e43719))
* removed ContentDefinition ([91ec34f](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/commit/91ec34f8cf2de045c4d0a03c7f2718d73fb86bf7))


### BREAKING CHANGES

* ContentDefinition no longer available, use nldocspec AnyResource instead

# [2.15.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/compare/2.14.0...2.15.0) (2024-10-22)


### Bug Fixes

* added typing to typeadapters ([24a9741](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/commit/24a974117eba83dac5937f8d08e06551761071aa))


### Features

* now publishing discriminated versions of descriptors and resources ([d7628d5](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/commit/d7628d5c3728808454a07618e8f95d97249ebffd))

# [2.14.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/compare/2.13.1...2.14.0) (2024-10-22)


### Bug Fixes

* made a typing.Union from the AnyResource definition ([8548a94](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/commit/8548a940d7667c4ea5f94132f74d13a034f81669))


### Features

* expose uri utils from root ([e3336d4](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/commit/e3336d42b212d83dfcc5abf2ca0db6b4cf8e8031))

## [2.13.1](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/compare/2.13.0...2.13.1) (2024-10-22)


### Bug Fixes

* add safe guard for first_descriptor_with_uri ([b60fa96](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/commit/b60fa96036361cfeb624f121e4546d5b5e6f023f))

# [2.13.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/compare/2.12.0...2.13.0) (2024-10-18)


### Features

* added descriptor helper functions to Resource ([282fc34](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/commit/282fc34b62ace632f1be3492042f4c2f586d65b0))

# [2.12.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/compare/2.11.0...2.12.0) (2024-10-16)


### Features

* added has_label nad has_attribute to Resource ([72173af](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/commit/72173af3fb2f4092971ad53442b86166531fbe0a))

# [2.11.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/compare/2.10.0...2.11.0) (2024-10-16)


### Features

* adde descriptor regex methods ([9059df6](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/commit/9059df6f7039a079544f08bb1621da1c7bcac30d))

# [2.10.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/compare/2.9.1...2.10.0) (2024-10-07)


### Features

* added get_bounding_box() func to resource ([967c163](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/commit/967c1630a1b8832b99d13eb554436a2a76eba6ed))

## [2.9.1](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/compare/2.9.0...2.9.1) (2024-10-07)


### Bug Fixes

* default for number_descriptor is now a float ([e519d6d](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/commit/e519d6d0bd20202d89e41431f9c0483d3c1674b2))

# [2.9.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/compare/2.8.0...2.9.0) (2024-10-07)


### Features

* added certain value functions to resources ([5768a54](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/commit/5768a54ee8f839a4dfd596fbd45ad285490f57b4))

# [2.8.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/compare/2.7.0...2.8.0) (2024-10-04)


### Bug Fixes

* correct class-checking of decsriptors ([a7029ef](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/commit/a7029efc8396ddeac652cf69390bb900f2bd9ec6))
* removed alpha commits from changelog ([d8c92b9](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/commit/d8c92b9fc4d2d65432dcc339e5be31375053c155))


### Features

* added descriptor utils ([120a951](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/commit/120a951d2c2aca041901847f2063743706dd7cab))

# [2.7.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/compare/2.6.3...2.7.0) (2024-10-03)

### Bug Fixes

- added ABC as parent class for all descriptors ([f47e79d](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/commit/f47e79de192aa6293a25df8bcb26ca3222b9f36e))
- added default text styling ([84894b5](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/commit/84894b5164b19002e7e412c37920b4e345b6c25e))
- added empty **init** file to have the module recognize the folder ([c12e168](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/commit/c12e16857a9707d9c71023d51ff11f7cbebbfca3))
- added imports ([a531990](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/commit/a53199006a21e7629364f08370209b4318a96335))
- correct imports ([5f5fc8f](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/commit/5f5fc8fadb0c3eecfaa6fffdbf2a6b103feb13af))
- create an id for all resources on init ([830d245](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/commit/830d2455f13f8e8b9e31f409566d9fd571a425f8))
- delay imports to prevent circularity ([9e0755f](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/commit/9e0755fa4f909bc1abc6e806131b2212d9c28a65))
- descriptor is no longer a resource ([364e057](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/commit/364e0579a643d94801f4f1a1f1711881d9dfd0a5))
- fixed import for nldocspecuri ([0251437](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/commit/0251437575d127c7ea972f79cae61a1f9dbe1936))
- no longer extending Abstract Base Class ([9b489c3](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/commit/9b489c33e5d037c765ca1c7a390e7b5c9da8c975))
- put anydescriptor in separate file ([ffa10af](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/commit/ffa10afd091a784b1d5355a8bba08e6685b2323c))
- regular import for AnyDescriptor ([79b41af](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/commit/79b41af686e90c5fef138b0170aa2351dd59bcc2))
- removed cache from bounding_box_to_dict ([cc450a7](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/commit/cc450a7ebd4c0423f940b1c03e716c266a90e55f))
- removed circular references in imports ([0f215bd](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/commit/0f215bd9907d88c29dab9376887423ca8773cf54))
- removed double declarations ([7a43b33](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/commit/7a43b33e22dbe642ef71e7c11606566dece70333))
- removed prefix "type" from alias ([a8f4944](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/commit/a8f4944b6cdb486563cbb0f85b00ff10393f3f37))
- removed typing from objectDescriptor dict ([eabfa43](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/commit/eabfa435322d6aa78659e828735bd1c621877130))
- removed unneeded imports ([d59c666](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/commit/d59c6669b8e8a1786c9334375aeefc9642220ddb))
- set predefined value for type, so that it isnt required on init ([10df73d](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/commit/10df73dbb59cb2779635f97ce05c04c64395af5c))

### Features

- added AnyDescriptor ([38437d5](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/commit/38437d553424b7477a30a247c706a060948ae299))
- added AnyResource ([499034f](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/commit/499034f084b8145ef86eba65e6d850ec49c9f44c))
- added bounding box conversion method ([3b30070](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/commit/3b300709e9d9e5bf10ee41aca5e36c762aac8f4a))
- added bounding_box_to_dict to utils ([5caf3c3](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/commit/5caf3c31eeea6afe49df494cfbd93f893cb63136))
- added nldocspec entities ([59c9702](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/commit/59c9702368af5d07fd5a7e676d3f5019e5c5867a))
- added nldocspec objects with pydantic ([114bf5c](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/commit/114bf5c51975c99ab1ab1cdc6a4cf437422beeba))
- added paragraph nldocspec entity ([8dfbc62](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/commit/8dfbc62a80e1e3d0f5201daa6fa65a32d5f34352))
- added uri functions ([2422867](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/commit/242286708f710496d2020c20914e987848a5e4e4))
- explicit export of descriptors and utils ([15cdcb2](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/commit/15cdcb28894b49b43bd6d784f3e3d79d44241d74))
- explicit properties exports ([fe7c8c8](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/commit/fe7c8c8473feeed3fb1b4818196c7bc7f47f682f))
- expose utils ([a945ccc](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/commit/a945cccae2fedc13d651a8116623b1a00f3a05c3))
- moved descriptors to resource, added helper functions ([e69ca46](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/commit/e69ca46bea352a794c43d6bc6480eb5abe6a15d5))
- objectDescriptor now uses object instead of dict ([e8aecef](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/commit/e8aecef94f712a586498938162c39a7e191101ec))
- small optimization ([c416675](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/commit/c416675ccd34900a60a9048c7dfe9e6bfc345238))
- validating nldocspec json, corrected spec to fully match documentation ([010157d](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/commit/010157d08ec48c3ee5107cc273d4af0f508c4725))

## [2.6.3](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/compare/2.6.2...2.6.3) (2024-09-04)

### Bug Fixes

- fixed version for setuptools ([7e72bd4](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/commit/7e72bd454c4c797b9940ddd52e881743d4bca603))

## [2.6.2](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/compare/2.6.1...2.6.2) (2024-09-04)

### Bug Fixes

- remove pkg_resources ([6038384](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/commit/60383847b85fa66050a5dde4aebc361e48aa7862))

## [2.6.1](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/compare/2.6.0...2.6.1) (2024-08-28)

### Bug Fixes

- communicate reconnect waiting time in log ([8b0c86f](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/commit/8b0c86f92009d5f6ee3c06afbf91bf75128773f9))

# [2.6.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/compare/2.5.3...2.6.0) (2024-08-28)

### Features

- use worker instance name as product name ([7ddd865](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/commit/7ddd865f46787835185b74da0f6ec5194932fc5b))

## [2.5.3](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/compare/2.5.2...2.5.3) (2024-08-27)

### Bug Fixes

- version 2.5.3 ([e2e5c2c](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/commit/e2e5c2ceaa8a10e08d1ddb68a5568273540bb800))

## [2.5.2](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/compare/2.5.1...2.5.2) (2024-08-27)

### Bug Fixes

- version 2.6.1 ([5e05b43](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/commit/5e05b43881fc0846915d58abf25eda45a34cd9b5))

## [2.5.1](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/compare/2.5.0...2.5.1) (2024-08-27)

### Bug Fixes

- update version.py to 2.6.0 ([b5d5c78](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/commit/b5d5c7861ffaec9e7039a532598f19790a4ca25d))

# [2.5.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/compare/2.4.0...2.5.0) (2024-08-13)

### Features

- specify python-package-name: kimiworker for ci ([1e6b20c](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/commit/1e6b20c9bbb7142e2fb73011746e9f7ee120b417))

# [2.4.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/compare/2.3.1...2.4.0) (2024-07-03)

### Features

- report version number in client properties ([7374bd9](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/commit/7374bd93692dbfa0f814f71980d384619bd6ee5c))

## [2.3.1](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/compare/2.3.0...2.3.1) (2024-06-04)

### Bug Fixes

- changed some logs to debug level ([5a2c4a5](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/commit/5a2c4a5ab6be862c033b1df8e1cdfa0f21682c69))

# [2.3.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/compare/2.2.1...2.3.0) (2024-05-23)

### Features

- send baseWorker version in logger ([f83c29b](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/commit/f83c29bf16bb7f397574fe0e1330da70d22edfc0))

## [2.2.1](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/compare/2.2.0...2.2.1) (2024-05-23)

### Bug Fixes

- import local file in setup.py ([7572610](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/commit/7572610eede5fc0e0a9b0f9b02478e71f5a4f328))

# [2.2.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/compare/2.1.0...2.2.0) (2024-05-23)

### Bug Fixes

- ack worker scout message to prevent unacked message blocking queue ([d3eb9d5](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/commit/d3eb9d5d13f04c2ab394c2779e5ae596f8b1573e))
- add pathdict and shapely to setup ([ac3ba7f](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/commit/ac3ba7f387567ceb0dae9c588434bf91fe464137))
- confidence is now a float instead of an int ([b73d651](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/commit/b73d651733f10e50c28aa6354cab82a7055f75d7))
- ensure backwards compatibility ([8def550](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/commit/8def5507805ed4acf8a5f06467f3dcb34594d2a7))
- imports ([3e63c31](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/commit/3e63c316abb82143c57f0c7473a1f5bedb2136ee))
- removed old statements ([e1ac874](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/commit/e1ac8742921873542fc83a8f60c065e3cd46250f))

### Features

- add bbox utilities ([f34a9c7](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/commit/f34a9c7edd54f28156253c1e5be347ed04523d5a))
- add utils ([66d7da9](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/commit/66d7da9ea408b5655b1edf09e8c9ea04e447b452))
- read version from version.py ([30fd110](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/commit/30fd1101cc747f4cd5d11a3f6bbb60eda13dc6ec))
- test worker ([c400e31](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/commit/c400e315a4e719cbea529d20b73c108a606cd2cc))

# [2.1.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/compare/2.0.2...2.1.0) (2024-05-17)

### Features

- exclude labels ([21f7671](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/commit/21f76710de5d5b5e2bde980d853150f18c47a09d))

## [2.0.2](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/compare/2.0.1...2.0.2) (2024-05-13)

### Bug Fixes

- send ack for worker scout messages ([e5cfbb0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/commit/e5cfbb083ee052ba28d9b8bac1777d52ed1ac840))

## [2.0.1](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/compare/2.0.0...2.0.1) (2024-04-30)

### Bug Fixes

- add pathdict and shapely to setup ([4462821](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/commit/4462821fbba2d9a6fd50da77b88d4972a5aa4eae))

# [2.0.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/compare/1.2.2...2.0.0) (2024-04-30)

- feat!: add worker scout ([d051fa4](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/commit/d051fa4e78761433b86ceb0b54a2925adf5e7984))

### BREAKING CHANGES

- workers now report health on a separate exchange and respond to scouting messages from the station

## [1.2.2](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/compare/v1.2.1...1.2.2) (2024-04-24)

### Bug Fixes

- force release ([36c84e6](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/commit/36c84e653c692037df9ea6f8b734e77f21584729))

## [1.2.1](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/compare/v1.2.0...v1.2.1) (2024-04-17)

### Bug Fixes

- NLDOC-1574 send correct content_encoding and content_type ([c9a5a3b](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/commit/c9a5a3b2f8b1fd0b59cc40c11cc0bfcc336bc9f9))

# [1.2.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/compare/v1.1.0...v1.2.0) (2024-04-08)

### Bug Fixes

- confidence is a float ([5c1aecf](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/commit/5c1aecf401547a481d0a8488ba354602a1a8f88a))

### Features

- add release step ([f270862](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/commit/f27086237d48bfc32474e309db6d7692aea15e58))
