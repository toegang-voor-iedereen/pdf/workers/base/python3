# setup.py
import os
from setuptools import setup, find_packages
from distutils.util import convert_path

# We need to import the version file to get the version number like this,
# as the module isn't loaded in the traditional way here, so we cannot
# do local imports

version_file = {}
ver_path = convert_path("kimiworker/version.py")
with open(ver_path) as ver_file:
    exec(ver_file.read(), version_file)

setup(
    name="kimiworker",
    author="MinBZK",
    description="A python3 library serving as a base for all python-based PDF workers",
    packages=find_packages(),
    include_package_data=True,
    classifiers=[
        "Environment :: Web Environment",
        "Intended Audience :: Developers",
        "Operating System :: OS Independent" "Programming Language :: Python",
        "Programming Language :: Python :: 3.11",
        "Development Status :: 2 - Pre-Alpha",
    ],
    python_requires=">=3.11",
    setup_requires=["setuptools-git-versioning"],
    install_requires=[
        "wheel",
        "twine",
        "pika==1.3.2",
        "minio==7.2.15",
        "ecs-logging==2.2.0",
        "pathdict==0.0.9",
        "shapely==2.0.7",
        "pydantic",
    ],
    version=version_file["version"],
)
